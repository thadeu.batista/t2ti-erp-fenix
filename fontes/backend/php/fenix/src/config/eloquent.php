<?php
require './vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as Capsule;

class GerenteConexaoE
{
    private static $instance;
	public $capsule;
    public $entityManager;

    public static function getInstance()
    {
        if(self::$instance === null){
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function __construct()
    {
		$capsule = new Capsule;
		
		$capsule->addConnection([
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'fenix',
			'username'  => 'root',
			'password'  => 'root',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		]);

		$capsule->bootEloquent();
		$capsule->setAsGlobal();
				
		$this->entityManager = $capsule;
        return $this->entityManager;
    }
}