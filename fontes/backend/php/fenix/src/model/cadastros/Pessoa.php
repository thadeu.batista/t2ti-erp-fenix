<?php
/*******************************************************************************
Title: T2Ti ERP Fenix                                                                
Description: Model relacionado à tabela [PESSOA] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2020 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
declare(strict_types=1);

class Pessoa extends EloquentModel implements \JsonSerializable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'PESSOA';

    /**
     * Eager Loading - Relacionamentos que devem ser sempre carregados por padrão
     *
     * @var array
     */
	protected $with = ['cliente', 'pessoaFisica', 'listaPessoaContato'];

    /**
     * Relations
     */
    public function cliente()
    {
        return $this->hasOne(Cliente::class, 'ID_PESSOA', 'ID');
	}

    public function pessoaFisica()
    {
        return $this->hasOne(PessoaFisica::class, 'ID_PESSOA', 'ID');
	}

    public function listaPessoaContato()
    {
        return $this->hasMany(PessoaContato::class, 'ID_PESSOA', 'ID');
	}
	
    /**
     * @ORM\OneToOne(targetEntity="Cliente", mappedBy="pessoa", cascade={"persist", "remove"})
     */
//    private $cliente;

    /**
     * @ORM\OneToOne(targetEntity="Colaborador", mappedBy="pessoa", cascade={"persist", "remove"})
     */
//    private $colaborador;

    /**
     * @ORM\OneToOne(targetEntity="Contador", mappedBy="pessoa", cascade={"persist", "remove"})
     */
    // private $contador;

    /**
     * @ORM\OneToOne(targetEntity="Fornecedor", mappedBy="pessoa", cascade={"persist", "remove"})
     */
    // private $fornecedor;

    /**
     * @ORM\OneToOne(targetEntity="PessoaFisica", mappedBy="pessoa", cascade={"persist", "remove"})
     */
    // private $pessoaFisica;

    /**
     * @ORM\OneToOne(targetEntity="PessoaJuridica", mappedBy="pessoa", cascade={"persist", "remove"})
     */
    // private $pessoaJuridica;

    /**
     * @ORM\OneToOne(targetEntity="Transportadora", mappedBy="pessoa", cascade={"persist", "remove"})
     */
    // private $transportadora;

    /**
     * @ORM\OneToMany(targetEntity="PessoaContato", mappedBy="pessoa", cascade={"persist", "remove"})
     */
    // private $listaPessoaContato;

    /**
     * @ORM\OneToMany(targetEntity="PessoaEndereco", mappedBy="pessoa", cascade={"persist", "remove"})
     */
    // private $listaPessoaEndereco;

    /**
     * @ORM\OneToMany(targetEntity="PessoaTelefone", mappedBy="pessoa", cascade={"persist", "remove"})
     */
    // private $listaPessoaTelefone;


    /**
     * Gets e Sets
     */
    public function getIdAttribute() 
	{
		return $this->attributes['ID'];
	}

	public function setIdAttribute($id) 
	{
		$this->attributes['ID'] = $id;
	}

    public function getNomeAttribute() 
	{
		return $this->attributes['NOME'];
	}

	public function setNomeAttribute($nome) 
	{
		$this->attributes['NOME'] = $nome;
	}

    public function getTipoAttribute() 
	{
		return $this->attributes['TIPO'];
	}

	public function setTipoAttribute($tipo) 
	{
		$this->attributes['TIPO'] = $tipo;
	}

    public function getSiteAttribute() 
	{
		return $this->attributes['SITE'];
	}

	public function setSiteAttribute($site) 
	{
		$this->attributes['SITE'] = $site;
	}

    public function getEmailAttribute() 
	{
		return $this->attributes['EMAIL'];
	}

	public function setEmailAttribute($email) 
	{
		$this->attributes['EMAIL'] = $email;
	}

    public function getEhClienteAttribute() 
	{
		return $this->attributes['EH_CLIENTE'];
	}

	public function setEhClienteAttribute($ehCliente) 
	{
		$this->attributes['EH_CLIENTE'] = $ehCliente;
	}

    public function getEhFornecedorAttribute() 
	{
		return $this->attributes['EH_FORNECEDOR'];
	}

	public function setEhFornecedorAttribute($ehFornecedor) 
	{
		$this->attributes['EH_FORNECEDOR'] = $ehFornecedor;
	}

    public function getEhTransportadoraAttribute() 
	{
		return $this->attributes['EH_TRANSPORTADORA'];
	}

	public function setEhTransportadoraAttribute($ehTransportadora) 
	{
		$this->attributes['EH_TRANSPORTADORA'] = $ehTransportadora;
	}

    public function getEhColaboradorAttribute() 
	{
		return $this->attributes['EH_COLABORADOR'];
	}

	public function setEhColaboradorAttribute($ehColaborador) 
	{
		$this->attributes['EH_COLABORADOR'] = $ehColaborador;
	}

    public function getEhContadorAttribute() 
	{
		return $this->attributes['EH_CONTADOR'];
	}

	public function setEhContadorAttribute($ehContador) 
	{
		$this->attributes['EH_CONTADOR'] = $ehContador;
	}


    /**
     * Instanciar Objeto
     */
    public function instanciar($objetoJson)
    {
        if (isset($objetoJson)) {
            isset($objetoJson->id) ? $this->setIdAttribute($objetoJson->id) : $this->setIdAttribute(null);
            $this->mapear($objetoJson);
        }

		// if (isset($objetoJson->cliente)) {
        //     $cliente = new Cliente();
        //     $cliente->instanciar($objetoJson->cliente);
        //     $this->cliente = $cliente;
		// }

            // // vincular objetos
            // $banco = new Banco();
            // $banco->instanciar($objJson->banco);
            // $objEntidade->banco()->associate($banco);

		// if (isset($objetoJson->colaborador)) {
		// 	$this->setColaborador(new Colaborador($objetoJson->colaborador));
		// 	$this->getColaborador()->setPessoa($this);
		// }

		// if (isset($objetoJson->contador)) {
		// 	$this->setContador(new Contador($objetoJson->contador));
		// 	$this->getContador()->setPessoa($this);
		// }

		// if (isset($objetoJson->fornecedor)) {
		// 	$this->setFornecedor(new Fornecedor($objetoJson->fornecedor));
		// 	$this->getFornecedor()->setPessoa($this);
		// }

		// if (isset($objetoJson->pessoaFisica)) {
		// 	$this->setPessoaFisica(new PessoaFisica($objetoJson->pessoaFisica));
		// 	$this->getPessoaFisica()->setPessoa($this);
		// }

		// if (isset($objetoJson->pessoaJuridica)) {
		// 	$this->setPessoaJuridica(new PessoaJuridica($objetoJson->pessoaJuridica));
		// 	$this->getPessoaJuridica()->setPessoa($this);
		// }

		// if (isset($objetoJson->transportadora)) {
		// 	$this->setTransportadora(new Transportadora($objetoJson->transportadora));
		// 	$this->getTransportadora()->setPessoa($this);
		// }

		
		// $this->listaPessoaContato = new ArrayCollection();
		// $listaPessoaContatoJson = $objetoJson->listaPessoaContato;
		// if ($listaPessoaContatoJson != null) {
		// 	for ($i = 0; $i < count($listaPessoaContatoJson); $i++) {
		// 		$objeto = new PessoaContato($listaPessoaContatoJson[$i]);
		// 		$objeto->setPessoa($this);
		// 		$this->listaPessoaContato->add($objeto);
		// 	}
		// }

		// $this->listaPessoaEndereco = new ArrayCollection();
		// $listaPessoaEnderecoJson = $objetoJson->listaPessoaEndereco;
		// if ($listaPessoaEnderecoJson != null) {
		// 	for ($i = 0; $i < count($listaPessoaEnderecoJson); $i++) {
		// 		$objeto = new PessoaEndereco($listaPessoaEnderecoJson[$i]);
		// 		$objeto->setPessoa($this);
		// 		$this->listaPessoaEndereco->add($objeto);
		// 	}
		// }

		// $this->listaPessoaTelefone = new ArrayCollection();
		// $listaPessoaTelefoneJson = $objetoJson->listaPessoaTelefone;
		// if ($listaPessoaTelefoneJson != null) {
		// 	for ($i = 0; $i < count($listaPessoaTelefoneJson); $i++) {
		// 		$objeto = new PessoaTelefone($listaPessoaTelefoneJson[$i]);
		// 		$objeto->setPessoa($this);
		// 		$this->listaPessoaTelefone->add($objeto);
		// 	}
		// }

    }

    /**
     * Mapping
     */
    public function mapear($objeto)
    {
        if (isset($objeto)) {
			$this->setNomeAttribute($objeto->nome);
			$this->setTipoAttribute($objeto->tipo);
			$this->setSiteAttribute($objeto->site);
			$this->setEmailAttribute($objeto->email);
			$this->setEhClienteAttribute($objeto->ehCliente);
			$this->setEhFornecedorAttribute($objeto->ehFornecedor);
			$this->setEhTransportadoraAttribute($objeto->ehTransportadora);
			$this->setEhColaboradorAttribute($objeto->ehColaborador);
			$this->setEhContadorAttribute($objeto->ehContador);
		}
    }


    /**
     * Serialization
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
			'id' => $this->getIdAttribute(),
			'nome' => $this->getNomeAttribute(),
			'tipo' => $this->getTipoAttribute(),
			'site' => $this->getSiteAttribute(),
			'email' => $this->getEmailAttribute(),
			'ehCliente' => $this->getEhClienteAttribute(),
			'ehFornecedor' => $this->getEhFornecedorAttribute(),
			'ehTransportadora' => $this->getEhTransportadoraAttribute(),
			'ehColaborador' => $this->getEhColaboradorAttribute(),
			'ehContador' => $this->getEhContadorAttribute(),
			'cliente' => $this->cliente,
			// 'colaborador' => $this->getColaborador(),
			// 'contador' => $this->getContador(),
			// 'fornecedor' => $this->getFornecedor(),
			'pessoaFisica' => $this->pessoaFisica,
			// 'pessoaJuridica' => $this->getPessoaJuridica(),
			// 'transportadora' => $this->getTransportadora(),
			'listaPessoaContato' => $this->listaPessoaContato,
			// 'listaPessoaEndereco' => $this->getListaPessoaEndereco(),
			// 'listaPessoaTelefone' => $this->getListaPessoaTelefone(),
        ];
    }
}
