{*******************************************************************************
Title: T2Ti ERP Fenix                                                                
Description: Service relacionado à tabela [REQUISICAO_INTERNA_CABECALHO] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2020 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************}
unit RequisicaoInternaCabecalhoService;

interface

uses
  RequisicaoInternaCabecalho, Colaborador, 
  System.SysUtils, System.Generics.Collections, ServiceBase, MVCFramework.DataSet.Utils;

type

  TRequisicaoInternaCabecalhoService = class(TServiceBase)
  private
    class procedure AnexarObjetosVinculados(AListaRequisicaoInternaCabecalho: TObjectList<TRequisicaoInternaCabecalho>); overload;
    class procedure AnexarObjetosVinculados(ARequisicaoInternaCabecalho: TRequisicaoInternaCabecalho); overload;
  public
    class function ConsultarLista: TObjectList<TRequisicaoInternaCabecalho>;
    class function ConsultarListaFiltro(AWhere: string): TObjectList<TRequisicaoInternaCabecalho>;
    class function ConsultarObjeto(AId: Integer): TRequisicaoInternaCabecalho;
    class procedure Inserir(ARequisicaoInternaCabecalho: TRequisicaoInternaCabecalho);
    class function Alterar(ARequisicaoInternaCabecalho: TRequisicaoInternaCabecalho): Integer;
    class function Excluir(ARequisicaoInternaCabecalho: TRequisicaoInternaCabecalho): Integer;
  end;

var
  sql: string;


implementation

{ TRequisicaoInternaCabecalhoService }

class procedure TRequisicaoInternaCabecalhoService.AnexarObjetosVinculados(ARequisicaoInternaCabecalho: TRequisicaoInternaCabecalho);
begin
  // Colaborador
  sql := 'SELECT * FROM COLABORADOR WHERE ID = ' + ARequisicaoInternaCabecalho.IdColaborador.ToString;
  ARequisicaoInternaCabecalho.Colaborador := GetQuery(sql).AsObject<TColaborador>;

end;

class procedure TRequisicaoInternaCabecalhoService.AnexarObjetosVinculados(AListaRequisicaoInternaCabecalho: TObjectList<TRequisicaoInternaCabecalho>);
var
  RequisicaoInternaCabecalho: TRequisicaoInternaCabecalho;
begin
  for RequisicaoInternaCabecalho in AListaRequisicaoInternaCabecalho do
  begin
    AnexarObjetosVinculados(RequisicaoInternaCabecalho);
  end;
end;

class function TRequisicaoInternaCabecalhoService.ConsultarLista: TObjectList<TRequisicaoInternaCabecalho>;
begin
  sql := 'SELECT * FROM REQUISICAO_INTERNA_CABECALHO ORDER BY ID';
  try
    Result := GetQuery(sql).AsObjectList<TRequisicaoInternaCabecalho>;
    AnexarObjetosVinculados(Result);
  finally
    Query.Close;
    Query.Free;
  end;
end;

class function TRequisicaoInternaCabecalhoService.ConsultarListaFiltro(AWhere: string): TObjectList<TRequisicaoInternaCabecalho>;
begin
  sql := 'SELECT * FROM REQUISICAO_INTERNA_CABECALHO where ' + AWhere;
  try
    Result := GetQuery(sql).AsObjectList<TRequisicaoInternaCabecalho>;
    AnexarObjetosVinculados(Result);
  finally
    Query.Close;
    Query.Free;
  end;
end;

class function TRequisicaoInternaCabecalhoService.ConsultarObjeto(AId: Integer): TRequisicaoInternaCabecalho;
begin
  sql := 'SELECT * FROM REQUISICAO_INTERNA_CABECALHO WHERE ID = ' + IntToStr(AId);
  try
    GetQuery(sql);
    if not Query.Eof then
    begin
      Result := Query.AsObject<TRequisicaoInternaCabecalho>;
      AnexarObjetosVinculados(Result);
    end
    else
      Result := nil;
  finally
    Query.Close;
    Query.Free;
  end;
end;

class procedure TRequisicaoInternaCabecalhoService.Inserir(ARequisicaoInternaCabecalho: TRequisicaoInternaCabecalho);
begin
  ARequisicaoInternaCabecalho.ValidarInsercao;
  ARequisicaoInternaCabecalho.Id := InserirBase(ARequisicaoInternaCabecalho, 'REQUISICAO_INTERNA_CABECALHO');
  
end;

class function TRequisicaoInternaCabecalhoService.Alterar(ARequisicaoInternaCabecalho: TRequisicaoInternaCabecalho): Integer;
begin
  ARequisicaoInternaCabecalho.ValidarAlteracao;
  Result := AlterarBase(ARequisicaoInternaCabecalho, 'REQUISICAO_INTERNA_CABECALHO');
  
  
end;


class function TRequisicaoInternaCabecalhoService.Excluir(ARequisicaoInternaCabecalho: TRequisicaoInternaCabecalho): Integer;
begin
  ARequisicaoInternaCabecalho.ValidarExclusao;
  
  Result := ExcluirBase(ARequisicaoInternaCabecalho.Id, 'REQUISICAO_INTERNA_CABECALHO');
end;


end.
