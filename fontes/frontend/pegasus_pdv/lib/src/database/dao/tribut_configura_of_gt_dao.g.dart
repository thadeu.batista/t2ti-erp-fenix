// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tribut_configura_of_gt_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$TributConfiguraOfGtDaoMixin on DatabaseAccessor<AppDatabase> {
  $TributConfiguraOfGtsTable get tributConfiguraOfGts =>
      attachedDatabase.tributConfiguraOfGts;
  $TributIcmsUfsTable get tributIcmsUfs => attachedDatabase.tributIcmsUfs;
}
