// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'nfe_cabecalho_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$NfeCabecalhoDaoMixin on DatabaseAccessor<AppDatabase> {
  $NfeCabecalhosTable get nfeCabecalhos => attachedDatabase.nfeCabecalhos;
  $NfeAcessoXmlsTable get nfeAcessoXmls => attachedDatabase.nfeAcessoXmls;
  $NfeCteReferenciadosTable get nfeCteReferenciados =>
      attachedDatabase.nfeCteReferenciados;
  $NfeCupomFiscalReferenciadosTable get nfeCupomFiscalReferenciados =>
      attachedDatabase.nfeCupomFiscalReferenciados;
  $NfeDetalhesTable get nfeDetalhes => attachedDatabase.nfeDetalhes;
  $NfeNfReferenciadasTable get nfeNfReferenciadas =>
      attachedDatabase.nfeNfReferenciadas;
  $NfeProcessoReferenciadosTable get nfeProcessoReferenciados =>
      attachedDatabase.nfeProcessoReferenciados;
  $NfeProdRuralReferenciadasTable get nfeProdRuralReferenciadas =>
      attachedDatabase.nfeProdRuralReferenciadas;
  $NfeReferenciadasTable get nfeReferenciadas =>
      attachedDatabase.nfeReferenciadas;
}
