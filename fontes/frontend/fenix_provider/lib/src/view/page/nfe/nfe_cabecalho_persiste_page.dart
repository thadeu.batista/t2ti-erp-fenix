/*
Title: T2Ti ERP 3.0                                                                
Description: AbaMestre PersistePage relacionada à tabela [NFE_CABECALHO] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2021 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
import 'package:fenix/src/infra/constantes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_bootstrap/flutter_bootstrap.dart';

import 'package:fenix/src/infra/biblioteca.dart';

import 'package:fenix/src/model/model.dart';

import 'package:fenix/src/view/shared/view_util_lib.dart';
import 'package:fenix/src/view/shared/widgets_input.dart';
import 'package:fenix/src/view/shared/widgets_abas.dart';
import 'package:fenix/src/infra/atalhos_desktop_web.dart';

import 'package:fenix/src/view/shared/page/lookup_page.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:fenix/src/infra/valida_campo_formulario.dart';
// import 'package:intl/intl.dart';

class NfeCabecalhoPersistePage extends StatefulWidget {
  final NfeCabecalho nfeCabecalho;
  final GlobalKey<FormState> formKey;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final FocusNode foco;
  final Function salvarNfeCabecalhoCallBack;
  final Function atualizarNfeCabecalhoCallBack;

  const NfeCabecalhoPersistePage(
      {Key key, this.formKey, this.scaffoldKey, this.nfeCabecalho, this.foco, this.salvarNfeCabecalhoCallBack, this.atualizarNfeCabecalhoCallBack})
      : super(key: key);

  @override
  _NfeCabecalhoPersistePageState createState() => _NfeCabecalhoPersistePageState();
}

class _NfeCabecalhoPersistePageState extends State<NfeCabecalhoPersistePage> {
  Map<LogicalKeySet, Intent> _shortcutMap; 
  Map<Type, Action<Intent>> _actionMap;

  @override
  void initState() {
    super.initState();
    _shortcutMap = getAtalhosPersistePage();

    _actionMap = <Type, Action<Intent>>{
      AtalhoTelaIntent: CallbackAction<AtalhoTelaIntent>(
        onInvoke: _tratarAcoesAtalhos,
      ),
    };
  }

  void _tratarAcoesAtalhos(AtalhoTelaIntent intent) {
    switch (intent.type) {
      case AtalhoTelaType.salvar:
        widget.salvarNfeCabecalhoCallBack();
        break;
      default:
        break;
    }
  }
  
  @override
  Widget build(BuildContext context) {
    final _importaTributOperacaoFiscalController = TextEditingController();
    _importaTributOperacaoFiscalController.text = widget.nfeCabecalho?.tributOperacaoFiscal?.descricao ?? '';
    final _importaNaturezaOperacaoController = TextEditingController();
    _importaNaturezaOperacaoController.text = widget.nfeCabecalho?.naturezaOperacao ?? '';
    final _importaVendaCabecalhoController = TextEditingController();
    _importaVendaCabecalhoController.text = widget.nfeCabecalho?.vendaCabecalho?.id ?? '';
    final _baseCalculoIcmsController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.baseCalculoIcms ?? 0);
    final _valorIcmsController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorIcms ?? 0);
    final _baseCalculoIcmsStController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.baseCalculoIcmsSt ?? 0);
    final _valorIcmsStController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorIcmsSt ?? 0);
    final _valorCofinsController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorCofins ?? 0);
    final _valorIpiController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorIpi ?? 0);
    final _valorFreteController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorFrete ?? 0);
    final _valorSeguroController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorSeguro ?? 0);
    final _valorDespesasAcessoriasController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorDespesasAcessorias ?? 0);
    final _valorPisController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorPis ?? 0);
    final _valorDescontoController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorDesconto ?? 0);
    final _valorImpostoImportacaoController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorImpostoImportacao ?? 0);
    final _valorTotalProdutosController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorTotalProdutos ?? 0);
    final _valorTotalController = MoneyMaskedTextController(precision: Constantes.decimaisValor, initialValue: widget.nfeCabecalho?.valorTotal ?? 0);

    widget.nfeCabecalho.codigoModelo = "55";
    widget.nfeCabecalho.serie = "001";
    widget.nfeCabecalho.dataHoraEmissao = DateTime.now();

    return FocusableActionDetector(
      actions: _actionMap,
      shortcuts: _shortcutMap,
      child: Focus(
        child: Scaffold(
          drawerDragStartBehavior: DragStartBehavior.down,
          key: widget.scaffoldKey,
          body: SafeArea(
            top: false,
            bottom: false,
            child: Form(
              key: widget.formKey,
              autovalidateMode: AutovalidateMode.always,
              child: Scrollbar(
                child: SingleChildScrollView(
                  dragStartBehavior: DragStartBehavior.down,
                  padding: ViewUtilLib.paddingAbaPersistePage,
                  child: BootstrapContainer(
                    fluid: true,
                    decoration: BoxDecoration(color: Colors.white),
                    padding: Biblioteca.isTelaPequena(context) == true ? ViewUtilLib.paddingBootstrapContainerTelaPequena : ViewUtilLib.paddingBootstrapContainerTelaGrande,
                    children: <Widget>[			  			  
                      Divider(color: Colors.white,),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-6',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: TextFormField(
                                      controller: _importaTributOperacaoFiscalController,
                                      readOnly: true,
                                      decoration: getInputDecoration(
                                        'Importe a Operação Fiscal Vinculada',
                                        'Operação Fiscal *',
                                        false),
                                      onSaved: (String value) {
                                      },
                                      validator: ValidaCampoFormulario.validarObrigatorio,
                                      onChanged: (text) {
                                        widget.nfeCabecalho?.tributOperacaoFiscal?.descricao = text;
                                        widget.nfeCabecalho.naturezaOperacao = widget.nfeCabecalho?.tributOperacaoFiscal?.descricaoNaNf;
                                        paginaMestreDetalheFoiAlterada = true;
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 0,
                                  child: IconButton(
                                    tooltip: 'Importar Operação Fiscal',
                                    icon: ViewUtilLib.getIconBotaoLookup(),
                                    onPressed: () async {
                                      ///chamando o lookup
                                      Map<String, dynamic> _objetoJsonRetorno =
                                        await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                              LookupPage(
                                                title: 'Importar Operação Fiscal',
                                                colunas: TributOperacaoFiscal.colunas,
                                                campos: TributOperacaoFiscal.campos,
                                                rota: '/tribut-operacao-fiscal/',
                                                campoPesquisaPadrao: 'descricao',
                                                valorPesquisaPadrao: '%',
                                              ),
                                              fullscreenDialog: true,
                                            ));
                                      if (_objetoJsonRetorno != null) {
                                        if (_objetoJsonRetorno['descricao'] != null) {
                                          _importaTributOperacaoFiscalController.text = _objetoJsonRetorno['descricao'];
                                          widget.nfeCabecalho.idTributOperacaoFiscal = _objetoJsonRetorno['id'];
                                          widget.nfeCabecalho.tributOperacaoFiscal = new TributOperacaoFiscal.fromJson(_objetoJsonRetorno);
                                          _importaNaturezaOperacaoController.text = widget.nfeCabecalho.tributOperacaoFiscal.descricaoNaNf;
                                        }
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-6',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: TextFormField(
                                      controller: _importaVendaCabecalhoController,
                                      readOnly: true,
                                      //validator: ValidaCampoFormulario.validarObrigatorio,
                                      decoration: getInputDecoration(
                                        'Importe a Venda Vinculada',
                                        'Venda *',
                                        false),
                                      onSaved: (String value) {
                                      },
                                      onChanged: (text) {
                                        widget.nfeCabecalho?.vendaCabecalho?.id = int.tryParse(text);
                                        paginaMestreDetalheFoiAlterada = true;
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 0,
                                  child: IconButton(
                                    tooltip: 'Importar Venda',
                                    icon: ViewUtilLib.getIconBotaoLookup(),
                                    onPressed: () async {
                                      ///chamando o lookup
                                      Map<String, dynamic> _objetoJsonRetorno =
                                        await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                              LookupPage(
                                                title: 'Importar Venda',
                                                colunas: VendaCabecalho.colunas,
                                                campos: VendaCabecalho.campos,
                                                rota: '/venda-cabecalho/',
                                                campoPesquisaPadrao: 'id',
                                                valorPesquisaPadrao: '%',
                                              ),
                                              fullscreenDialog: true,
                                            ));
                                      if (_objetoJsonRetorno != null) {
                                        if (_objetoJsonRetorno['id'] != null) {
                                          _importaVendaCabecalhoController.text = _objetoJsonRetorno['id'];
                                          widget.nfeCabecalho.idVendaCabecalho = _objetoJsonRetorno['id'];
                                          widget.nfeCabecalho.vendaCabecalho = new VendaCabecalho.fromJson(_objetoJsonRetorno);
                                        }
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                            ),
                          ),
                        ],
                      ),
                      Divider(color: Colors.white,),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho.codigoModelo + ' - NF-e',
                                readOnly: true,
                                decoration: getInputDecoration(
                                  'Informe o Código do Modelo',
                                  'Código Modelo *',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                controller: _importaNaturezaOperacaoController,
                                maxLength: 60,
                                maxLines: 1,
                                readOnly: true,
                                validator: ValidaCampoFormulario.validarObrigatorio,
                                //initialValue: widget.nfeCabecalho?.naturezaOperacao ?? '',
                                decoration: getInputDecoration(
                                  'Informe a Natureza da Operação',
                                  'Natureza da Operação',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                readOnly: true,
                                maxLength: 44,
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho?.chaveAcesso ?? '',
                                decoration: getInputDecoration(
                                  'Chave de Acesso - Gerado Automaticamente',
                                  'Chave de Acesso',
                                  false),
                                onSaved: (String value) {
                                },
                                onChanged: (text) {
                                  widget.nfeCabecalho.chaveAcesso = text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(color: Colors.white,),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                readOnly: true,
                                maxLength: 8,
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho?.codigoNumerico ?? '',
                                decoration: getInputDecoration(
                                  'Código Numérico - Gerado Automaticamente',
                                  'Código Numérico',
                                  false),
                                onSaved: (String value) {
                                },
                                onChanged: (text) {
                                  widget.nfeCabecalho.codigoNumerico = text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                maxLength: 3,
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho?.serie ?? '',
                                decoration: getInputDecoration(
                                  'Informe a Série',
                                  'Série',
                                  false),
                                onSaved: (String value) {
                                },
                                validator: ValidaCampoFormulario.validarObrigatorio,
                                onChanged: (text) {
                                  widget.nfeCabecalho.serie = text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                readOnly: true,
                                maxLength: 9,
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho?.numero ?? '',
                                decoration: getInputDecoration(
                                  'Número - Gerado Automaticamente',
                                  'Número',
                                  false),
                                onSaved: (String value) {
                                },
                                onChanged: (text) {
                                  widget.nfeCabecalho.numero = text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(color: Colors.white,),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: InputDecorator(
                                decoration: getInputDecoration(
                                  'Informe a Data/Hora Emissão',
                                  'Data/Hora Emissão *',
                                  true),
                                isEmpty: widget.nfeCabecalho.dataHoraEmissao == null,
                                child: DatePickerItem(
                                  readOnly: true,
                                  mascara: "dd/MM/yyyy H:m",
                                  dateTime: widget.nfeCabecalho.dataHoraEmissao,
                                  firstDate: DateTime.parse('1900-01-01'),
                                  lastDate: DateTime.now(),
                                  onChanged: (DateTime value) {
                                    paginaMestreDetalheFoiAlterada = true;
                                    setState(() {
                                      widget.nfeCabecalho.dataHoraEmissao = value;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: InputDecorator(
                                decoration: getInputDecoration(
                                  'Informe a Data/Hora Entrada/Saída',
                                  'Data/Hora Entrada/Saída',
                                  true),
                                isEmpty: widget.nfeCabecalho.dataHoraEntradaSaida == null,
                                child: DatePickerItem(
                                  mascara: "dd/MM/yyyy H:m",
                                  dateTime: widget.nfeCabecalho.dataHoraEntradaSaida,
                                  firstDate: DateTime.parse('1900-01-01'),
                                  lastDate: DateTime.now(),
                                  onChanged: (DateTime value) {
                                    paginaMestreDetalheFoiAlterada = true;
                                    setState(() {
                                      widget.nfeCabecalho.dataHoraEntradaSaida = value;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(color: Colors.white,),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: InputDecorator(
                                decoration: getInputDecoration(
                                  'Selecione a Opção Desejada',
                                  'Tipo Operação *',
                                  true),
                                isEmpty: widget.nfeCabecalho.tipoOperacao == null,
                                child: getDropDownButton(widget.nfeCabecalho.tipoOperacao,
                                  (String newValue) {
                                    paginaMestreDetalheFoiAlterada = true;
                                    setState(() {
                                      widget.nfeCabecalho.tipoOperacao = newValue;
                                    });
                                }, <String>[
                                  '0=Entrada',
                                  '1=Saída',
                              ])),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: InputDecorator(
                                decoration: getInputDecoration(
                                  'Selecione a Opção Desejada',
                                  'Tipo de Emissão *',
                                  true),
                                isEmpty: widget.nfeCabecalho.tipoEmissao == null,
                                child: getDropDownButton(widget.nfeCabecalho.tipoEmissao,
                                  (String newValue) {
                                    paginaMestreDetalheFoiAlterada = true;
                                    setState(() {
                                      widget.nfeCabecalho.tipoEmissao = newValue;
                                    });
                                }, <String>[
                                  '1=Emissão normal',
                                  '2=Contingência FS-IA',
                                  '4=Contingência EPEC',
                                  '5=Contingência FS-DA',
                                  '6=Contingência SVC-AN',
                                  '7=Contingência SVC-RS',
                              ])),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: InputDecorator(
                                decoration: getInputDecoration(
                                  'Selecione a Opção Desejada',
                                  'Formato de Impressão do DANFE *',
                                  true),
                                isEmpty: widget.nfeCabecalho.formatoImpressaoDanfe == null,
                                child: getDropDownButton(widget.nfeCabecalho.formatoImpressaoDanfe,
                                  (String newValue) {
                                    paginaMestreDetalheFoiAlterada = true;
                                    setState(() {
                                      widget.nfeCabecalho.formatoImpressaoDanfe = newValue;
                                    });
                                }, <String>[
                                  '0=Sem geração de DANFE',
                                  '1=DANFE normal, Retrato',
                                  '2=DANFE normal, Paisagem',
                                  '3=DANFE Simplificado',
                              ])),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: InputDecorator(
                                decoration: getInputDecoration(
                                  'Selecione a Opção Desejada',
                                  'Finalidade Emissão *',
                                  true),
                                isEmpty: widget.nfeCabecalho.finalidadeEmissao == null,
                                child: getDropDownButton(widget.nfeCabecalho.finalidadeEmissao,
                                  (String newValue) {
                                    paginaMestreDetalheFoiAlterada = true;
                                    setState(() {
                                      widget.nfeCabecalho.finalidadeEmissao = newValue;
                                    });
                                }, <String>[
                                  '1=NF-e normal',
                                  '2=NF-e complementar',
                                  '3=NF-e de ajuste',
                                  '4=Devolução de mercadoria',
                              ])),
                            ),
                          ),
                        ],
                      ),
                      Divider(color: Colors.white,),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _baseCalculoIcmsController,
                                decoration: getInputDecoration(
                                  'Informe a Base de Cálculo do ICMS',
                                  'Base Cálculo ICMS',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorIcmsController,
                                decoration: getInputDecoration(
                                  'Informe o Valor do ICMS',
                                  'Valor do ICMS',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _baseCalculoIcmsStController,
                                decoration: getInputDecoration(
                                  'Informe a Base de Cálculo do ICMS ST',
                                  'Base Cálculo ICMS ST',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorIcmsStController,
                                decoration: getInputDecoration(
                                  'Informe o Valor do ICMS ST',
                                  'Valor do ICMS ST',
                                  false),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(color: Colors.white,),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorCofinsController,
                                decoration: getInputDecoration(
                                  'Informe o Valor COFINS',
                                  'Valor COFINS',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorIpiController,
                                decoration: getInputDecoration(
                                  'Informe o Valor IPI',
                                  'Valor IPI',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorFreteController,
                                decoration: getInputDecoration(
                                  'Informe o Valor Frete',
                                  'Valor Frete',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorSeguroController,
                                decoration: getInputDecoration(
                                  'Informe o Valor Seguro',
                                  'Valor Seguro',
                                  false),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(color: Colors.white,),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorDespesasAcessoriasController,
                                decoration: getInputDecoration(
                                  'Informe o Valor Despesas Acessórias',
                                  'Valor Despesas Acessórias',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorPisController,
                                decoration: getInputDecoration(
                                  'Informe o Valor PIS',
                                  'Valor PIS',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorDescontoController,
                                decoration: getInputDecoration(
                                  'Informe o Valor Desconto',
                                  'Valor Desconto',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorImpostoImportacaoController,
                                decoration: getInputDecoration(
                                  'Informe o Valor Imposto Importação',
                                  'Valor Imposto Importação',
                                  false),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(color: Colors.white,),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorTotalProdutosController,
                                decoration: getInputDecoration(
                                  'Informe o Valor Total Produtos',
                                  'Valor Total Produtos',
                                  false),
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding: Biblioteca.distanciaEntreColunasQuebraLinha(context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                readOnly: true,
                                textAlign: TextAlign.end,
                                controller: _valorTotalController,
                                decoration: getInputDecoration(
                                  'Informe o Valor Total NF',
                                  'Valor Total NF',
                                  false),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(color: Colors.white,),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12',
                            child: 
                              Text(
                                '* indica que o campo é obrigatório',
                                style: Theme.of(context).textTheme.caption,
                              ),								
                          ),
                        ],
                      ),
                      Divider(color: Colors.white,),
                    ],
                  ),
                ),
              ),			  
            ),
          ),
        ),
      ),
    );
  }
}