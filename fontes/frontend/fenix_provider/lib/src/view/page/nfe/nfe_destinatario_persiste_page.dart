/*
Title: T2Ti ERP 3.0                                                                
Description: AbaMestre PersistePage OneToOne relacionada à tabela [NFE_DESTINATARIO] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2021 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_bootstrap/flutter_bootstrap.dart';

import 'package:fenix/src/infra/constantes.dart';
import 'package:fenix/src/infra/biblioteca.dart';

import 'package:fenix/src/model/model.dart';

import 'package:fenix/src/view/shared/view_util_lib.dart';
import 'package:fenix/src/view/shared/widgets_input.dart';
import 'package:fenix/src/view/shared/widgets_abas.dart';
import 'package:fenix/src/infra/atalhos_desktop_web.dart';

import 'package:fenix/src/view/shared/page/lookup_page.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:fenix/src/infra/valida_campo_formulario.dart';
import 'package:fenix/src/view/shared/dropdown_lista.dart';

class NfeDestinatarioPersistePage extends StatefulWidget {
  final NfeCabecalho nfeCabecalho;
  final GlobalKey<FormState> formKey;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final FocusNode foco;
  final Function salvarNfeCabecalhoCallBack;

  const NfeDestinatarioPersistePage(
      {Key key,
      this.formKey,
      this.scaffoldKey,
      this.nfeCabecalho,
      this.foco,
      this.salvarNfeCabecalhoCallBack})
      : super(key: key);

  @override
  _NfeDestinatarioPersistePageState createState() =>
      _NfeDestinatarioPersistePageState();
}

class _NfeDestinatarioPersistePageState
    extends State<NfeDestinatarioPersistePage> {
  Map<LogicalKeySet, Intent> _shortcutMap;
  Map<Type, Action<Intent>> _actionMap;

  @override
  void initState() {
    super.initState();
    _shortcutMap = getAtalhosPersistePage();

    _actionMap = <Type, Action<Intent>>{
      AtalhoTelaIntent: CallbackAction<AtalhoTelaIntent>(
        onInvoke: _tratarAcoesAtalhos,
      ),
    };
  }

  void _tratarAcoesAtalhos(AtalhoTelaIntent intent) {
    switch (intent.type) {
      case AtalhoTelaType.salvar:
        widget.salvarNfeCabecalhoCallBack();
        break;
      default:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    var _cpfController = MaskedTextController(
      mask: Constantes.mascaraCPF,
      text: widget.nfeCabecalho?.nfeDestinatario?.cpf ?? '',
    );
    var _cnpjController = MaskedTextController(
      mask: Constantes.mascaraCNPJ,
      text: widget.nfeCabecalho?.nfeDestinatario?.cnpj ?? '',
    );
    var _importaCepController = TextEditingController();
    _importaCepController.text =
        widget.nfeCabecalho?.nfeDestinatario?.cep ?? '';
    var _telefoneController = MaskedTextController(
      mask: Constantes.mascaraTELEFONE,
      text: widget.nfeCabecalho?.nfeDestinatario?.telefone ?? '',
    );

    if (widget.nfeCabecalho.nfeDestinatario == null) {
      widget.nfeCabecalho.nfeDestinatario = NfeDestinatario();
    }

    return FocusableActionDetector(
      actions: _actionMap,
      shortcuts: _shortcutMap,
      child: Focus(
        child: Scaffold(
          drawerDragStartBehavior: DragStartBehavior.down,
          key: widget.scaffoldKey,
          body: SafeArea(
            top: false,
            bottom: false,
            child: Form(
              key: widget.formKey,
              autovalidateMode: AutovalidateMode.always,
              child: Scrollbar(
                child: SingleChildScrollView(
                  dragStartBehavior: DragStartBehavior.down,
                  padding: ViewUtilLib.paddingAbaPersistePage,
                  child: BootstrapContainer(
                    fluid: true,
                    decoration: BoxDecoration(color: Colors.white),
                    padding: Biblioteca.isTelaPequena(context) == true ? ViewUtilLib.paddingBootstrapContainerTelaPequena : ViewUtilLib.paddingBootstrapContainerTelaGrande,
                    children: <Widget>[
                      Divider(
                        color: Colors.white,
                      ),
                      BootstrapRow(height: 60, children: <BootstrapCol>[
                        BootstrapCol(
                          sizes: 'col-12 col-md-3',
                          child: Padding(
                            padding:
                                Biblioteca.distanciaEntreColunasQuebraLinha(
                                    context),
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              focusNode: widget.foco,
                              autofocus: true,
                              controller: _cpfController,
                              decoration: getInputDecoration(
                                  'Informe o CPF', 'CPF', false),
                              onSaved: (String value) {},
                              validator: ValidaCampoFormulario.validarCPF,
                              onChanged: (text) {
                                widget.nfeCabecalho.nfeDestinatario?.cpf = text;
                                paginaMestreDetalheFoiAlterada = true;
                              },
                            ),
                          ),
                        ),
                        BootstrapCol(
                          sizes: 'col-12 col-md-3',
                          child: Padding(
                            padding:
                                Biblioteca.distanciaEntreColunasQuebraLinha(
                                    context),
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              controller: _cnpjController,
                              decoration: getInputDecoration(
                                  'Informe o CNPJ', 'CNPJ', false),
                              onSaved: (String value) {},
                              validator: ValidaCampoFormulario.validarCNPJ,
                              onChanged: (text) {
                                widget.nfeCabecalho.nfeDestinatario?.cnpj =
                                    text;
                                paginaMestreDetalheFoiAlterada = true;
                              },
                            ),
                          ),
                        ),
                        BootstrapCol(
                          sizes: 'col-12 col-md-6',
                          child: Padding(
                            padding:
                                Biblioteca.distanciaEntreColunasQuebraLinha(
                                    context),
                            child: TextFormField(
                              maxLength: 60,
                              maxLines: 1,
                              initialValue:
                                  widget.nfeCabecalho?.nfeDestinatario?.nome ??
                                      '',
                              decoration: getInputDecoration(
                                  'Informe o Nome', 'Nome *', false),
                              onSaved: (String value) {},
                              validator:
                                  ValidaCampoFormulario.validarObrigatorio,
                              onChanged: (text) {
                                widget.nfeCabecalho.nfeDestinatario?.nome =
                                    text;
                                paginaMestreDetalheFoiAlterada = true;
                              },
                            ),
                          ),
                        ),
                      ]),
                      Divider(
                        color: Colors.white,
                      ),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding:
                                  Biblioteca.distanciaEntreColunasQuebraLinha(
                                      context),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      child: TextFormField(
                                        controller: _importaCepController,
                                        readOnly: true,
                                        decoration:
                                            getInputDecoration(
                                                'Importe o CEP Vinculado',
                                                'CEP *',
                                                false),
                                        onSaved: (String value) {},
                                        validator: ValidaCampoFormulario
                                            .validarObrigatorio,
                                        onChanged: (text) {
                                          widget.nfeCabecalho?.nfeDestinatario
                                              ?.cep = text;
                                          paginaMestreDetalheFoiAlterada = true;
                                        },
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 0,
                                    child: IconButton(
                                      tooltip: 'Importar CEP',
                                      icon: ViewUtilLib.getIconBotaoLookup(),
                                      onPressed: () async {
                                        ///chamando o lookup
                                        Map<String, dynamic>
                                            _objetoJsonRetorno =
                                            await Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          LookupPage(
                                                    title: 'Importar CEP',
                                                    colunas: Cep.colunas,
                                                    campos: Cep.campos,
                                                    rota: '/cep/',
                                                    campoPesquisaPadrao:
                                                        'numero',
                                                    valorPesquisaPadrao: '',
                                                  ),
                                                  fullscreenDialog: true,
                                                ));
                                        if (_objetoJsonRetorno != null) {
                                          if (_objetoJsonRetorno['numero'] !=
                                              null) {
                                            _importaCepController.text =
                                                _objetoJsonRetorno['numero'];
                                            widget.nfeCabecalho.nfeDestinatario
                                                    ?.cep =
                                                _objetoJsonRetorno['numero'];
                                          }
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-6',
                            child: Padding(
                              padding:
                                  Biblioteca.distanciaEntreColunasQuebraLinha(
                                      context),
                              child: TextFormField(
                                maxLength: 60,
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho
                                        ?.nfeDestinatario?.logradouro ??
                                    '',
                                decoration: getInputDecoration(
                                    'Informe o Logradouro',
                                    'Logradouro',
                                    false),
                                onSaved: (String value) {},
                                onChanged: (text) {
                                  widget.nfeCabecalho.nfeDestinatario
                                      ?.logradouro = text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding:
                                  Biblioteca.distanciaEntreColunasQuebraLinha(
                                      context),
                              child: TextFormField(
                                maxLength: 60,
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho
                                        ?.nfeDestinatario?.numero ??
                                    '',
                                decoration: getInputDecoration(
                                    'Informe o Número', 'Número', false),
                                onSaved: (String value) {},
                                onChanged: (text) {
                                  widget.nfeCabecalho.nfeDestinatario?.numero =
                                      text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.white,
                      ),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding:
                                  Biblioteca.distanciaEntreColunasQuebraLinha(
                                      context),
                              child: TextFormField(
                                maxLength: 60,
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho
                                        ?.nfeDestinatario?.complemento ??
                                    '',
                                decoration: getInputDecoration(
                                    'Informe o Complemento',
                                    'Complemento',
                                    false),
                                onSaved: (String value) {},
                                onChanged: (text) {
                                  widget.nfeCabecalho.nfeDestinatario
                                      ?.complemento = text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding:
                                  Biblioteca.distanciaEntreColunasQuebraLinha(
                                      context),
                              child: TextFormField(
                                maxLength: 60,
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho
                                        ?.nfeDestinatario?.bairro ??
                                    '',
                                decoration: getInputDecoration(
                                    'Informe o Bairro', 'Bairro', false),
                                onSaved: (String value) {},
                                onChanged: (text) {
                                  widget.nfeCabecalho.nfeDestinatario?.bairro =
                                      text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding:
                                  Biblioteca.distanciaEntreColunasQuebraLinha(
                                      context),
                              child: TextFormField(
                                readOnly: true,
                                maxLength: 60,
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho
                                        ?.nfeDestinatario?.nomeMunicipio ??
                                    '',
                                decoration: getInputDecoration(
                                    'Informe o Município', 'Município', false),
                                onSaved: (String value) {},
                                onChanged: (text) {
                                  widget.nfeCabecalho.nfeDestinatario
                                      ?.nomeMunicipio = text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.white,
                      ),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12 col-md-2',
                            child: Padding(
                              padding:
                                  Biblioteca.distanciaEntreColunasQuebraLinha(
                                      context),
                              child: InputDecorator(
                                  decoration: getInputDecoration(
                                      'Informe a UF', 'UF', true),
                                  isEmpty: widget.nfeCabecalho?.nfeDestinatario
                                              ?.uf ==
                                          null ||
                                      widget.nfeCabecalho?.nfeDestinatario ==
                                          null,
                                  child: getDropDownButton(
                                      widget.nfeCabecalho.nfeDestinatario?.uf,
                                      (String newValue) {
                                    paginaMestreDetalheFoiAlterada = true;
                                    setState(() {
                                      widget.nfeCabecalho.nfeDestinatario?.uf =
                                          newValue;
                                    });
                                  }, DropdownLista.listaUF)),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding:
                                  Biblioteca.distanciaEntreColunasQuebraLinha(
                                      context),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                controller: _telefoneController,
                                decoration: getInputDecoration(
                                    'Informe o Telefone', 'Telefone', false),
                                onSaved: (String value) {},
                                //validator: ValidaCampoFormulario.validarTelefone,
                                onChanged: (text) {
                                  widget.nfeCabecalho.nfeDestinatario
                                      ?.telefone = text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-3',
                            child: Padding(
                              padding:
                                  Biblioteca.distanciaEntreColunasQuebraLinha(
                                      context),
                              child: TextFormField(
                                maxLength: 14,
                                maxLines: 1,
                                initialValue: widget.nfeCabecalho
                                        ?.nfeDestinatario?.inscricaoEstadual ??
                                    '',
                                decoration: getInputDecoration(
                                    'Informe o Inscrição Estadual',
                                    'Inscrição Estadual',
                                    false),
                                onSaved: (String value) {},
                                onChanged: (text) {
                                  widget.nfeCabecalho.nfeDestinatario
                                      ?.inscricaoEstadual = text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                          BootstrapCol(
                            sizes: 'col-12 col-md-4',
                            child: Padding(
                              padding:
                                  Biblioteca.distanciaEntreColunasQuebraLinha(
                                      context),
                              child: TextFormField(
                                maxLength: 60,
                                maxLines: 1,
                                initialValue: widget
                                        .nfeCabecalho?.nfeDestinatario?.email ??
                                    '',
                                decoration: getInputDecoration(
                                    'Informe o EMail', 'EMail', false),
                                onSaved: (String value) {},
                                onChanged: (text) {
                                  widget.nfeCabecalho.nfeDestinatario?.email =
                                      text;
                                  paginaMestreDetalheFoiAlterada = true;
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.white,
                      ),
                      BootstrapRow(
                        height: 60,
                        children: <BootstrapCol>[
                          BootstrapCol(
                            sizes: 'col-12',
                            child: Text(
                              '* indica que o campo é obrigatório',
                              style: Theme.of(context).textTheme.caption,
                            ),
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
