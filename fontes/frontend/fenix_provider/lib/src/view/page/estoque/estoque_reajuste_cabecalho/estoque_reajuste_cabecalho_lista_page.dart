/*
Title: T2Ti ERP 3.0                                                                
Description: AbaMestre ListaPage relacionada à tabela [ESTOQUE_REAJUSTE_CABECALHO] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2021 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart';

import 'package:fenix/src/infra/sessao.dart';
import 'package:fenix/src/infra/constantes.dart';

import 'package:fenix/src/view/shared/view_util_lib.dart';
import 'package:fenix/src/view/shared/botoes.dart';
import 'package:fenix/src/infra/atalhos_desktop_web.dart';
import 'package:fenix/src/view/shared/caixas_de_dialogo.dart';

import 'package:fenix/src/model/model.dart';
import 'package:fenix/src/model/filtro.dart';
import 'package:fenix/src/view_model/view_model.dart';

import 'package:fenix/src/view/shared/page/filtro_page.dart';
import 'package:fenix/src/view/shared/page/pdf_page.dart';

import 'package:intl/intl.dart';
import 'estoque_reajuste_cabecalho_page.dart';

class EstoqueReajusteCabecalhoListaPage extends StatefulWidget {
  @override
  _EstoqueReajusteCabecalhoListaPageState createState() => _EstoqueReajusteCabecalhoListaPageState();
}

class _EstoqueReajusteCabecalhoListaPageState extends State<EstoqueReajusteCabecalhoListaPage> {
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;
  int _sortColumnIndex;
  bool _sortAscending = true;
  var _filtro = Filtro();
  final _colunas = EstoqueReajusteCabecalho.colunas;
  final _campos = EstoqueReajusteCabecalho.campos;

  Map<LogicalKeySet, Intent> _shortcutMap; 
  Map<Type, Action<Intent>> _actionMap;

  @override
  void initState() {
    super.initState();
    _shortcutMap = getAtalhosListaPage();

    _actionMap = <Type, Action<Intent>>{
      AtalhoTelaIntent: CallbackAction<AtalhoTelaIntent>(
        onInvoke: _tratarAcoesAtalhos,
      ),
    };
  }

  @override
  void didChangeDependencies() {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => Sessao.tratarErrosSessao(context, Provider.of<EstoqueReajusteCabecalhoViewModel>(context, listen: false).objetoJsonErro)
    );
    super.didChangeDependencies();
  }

  void _tratarAcoesAtalhos(AtalhoTelaIntent intent) {
    switch (intent.type) {
      case AtalhoTelaType.inserir:
        _inserir();
        break;
      case AtalhoTelaType.imprimir:
        _gerarRelatorio();
        break;
      case AtalhoTelaType.filtrar:
        _chamarFiltro();
        break;
      default:
        break;
    }
  }
  
  @override
  Widget build(BuildContext context) {
    final _listaEstoqueReajusteCabecalho = Provider.of<EstoqueReajusteCabecalhoViewModel>(context).listaEstoqueReajusteCabecalho;
    final _objetoJsonErro = Provider.of<EstoqueReajusteCabecalhoViewModel>(context).objetoJsonErro;

    if (_listaEstoqueReajusteCabecalho == null && _objetoJsonErro == null) {
      Provider.of<EstoqueReajusteCabecalhoViewModel>(context, listen: false).consultarLista();
    }

    final _EstoqueReajusteCabecalhoDataSource _estoqueReajusteCabecalhoDataSource = _EstoqueReajusteCabecalhoDataSource(_listaEstoqueReajusteCabecalho, context);

    void _sort<T>(Comparable<T> getField(EstoqueReajusteCabecalho estoqueReajusteCabecalho), int columnIndex, bool ascending) {
      _estoqueReajusteCabecalhoDataSource._sort<T>(getField, ascending);
      setState(() {
        _sortColumnIndex = columnIndex;
        _sortAscending = ascending;
      });
    }

      return FocusableActionDetector(
        actions: _actionMap,
        shortcuts: _shortcutMap,
        child: Focus(
          autofocus: true,
          child: Scaffold(
            appBar: AppBar(
              title: const Text('Cadastro - Estoque Reajuste Cabecalho'),
              actions: <Widget>[],
            ),
            floatingActionButton: FloatingActionButton(
              focusColor: ViewUtilLib.getBotaoFocusColor(),
              tooltip: Constantes.botaoInserirDica,
              backgroundColor: ViewUtilLib.getBackgroundColorBotaoInserir(),
              child: ViewUtilLib.getIconBotaoInserir(),
              onPressed: () {
                _inserir();
              }),
            floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
            bottomNavigationBar: BottomAppBar(
              color: ViewUtilLib.getBottomAppBarColor(),
              shape: CircularNotchedRectangle(),
              child: Row(
                children: getBotoesNavigationBarListaPage(
                  context: context, 
                  chamarFiltro: _chamarFiltro, 
                  gerarRelatorio: _gerarRelatorio,
                ),
              ),
            ),
            body: RefreshIndicator(
              onRefresh: _refrescarTela,
              child: Scrollbar(
                child: _listaEstoqueReajusteCabecalho == null
                ? Center(child: CircularProgressIndicator())
                : ListView(
                  padding: EdgeInsets.all(Constantes.paddingListViewListaPage),
                  children: <Widget>[
                    PaginatedDataTable(
                      header: const Text('Relação - Estoque Reajuste Cabecalho'),
                      rowsPerPage: _rowsPerPage,
                      onRowsPerPageChanged: (int value) {
                        setState(() {
                          _rowsPerPage = value;
                        });
                      },
                      sortColumnIndex: _sortColumnIndex,
                      sortAscending: _sortAscending,
                      columns: <DataColumn>[
                        DataColumn(
                          numeric: true,
                          label: const Text('Id'),
                          tooltip: 'Id',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((EstoqueReajusteCabecalho estoqueReajusteCabecalho) => estoqueReajusteCabecalho.id, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Colaborador'),
                          tooltip: 'Colaborador',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((EstoqueReajusteCabecalho estoqueReajusteCabecalho) => estoqueReajusteCabecalho.colaborador?.pessoa?.nome, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Data do Reajuste'),
                          tooltip: 'Data do Reajuste',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<DateTime>((EstoqueReajusteCabecalho estoqueReajusteCabecalho) => estoqueReajusteCabecalho.dataReajuste, columnIndex, ascending),
                        ),
                        DataColumn(
                          numeric: true,
                          label: const Text('Taxa Reajuste'),
                          tooltip: 'Taxa Reajuste',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((EstoqueReajusteCabecalho estoqueReajusteCabecalho) => estoqueReajusteCabecalho.taxa, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Tipo do Reajuste'),
                          tooltip: 'Tipo do Reajuste de Pessoa',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((EstoqueReajusteCabecalho estoqueReajusteCabecalho) => estoqueReajusteCabecalho.tipoReajuste, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Justificativa'),
                          tooltip: 'Justificativa',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((EstoqueReajusteCabecalho estoqueReajusteCabecalho) => estoqueReajusteCabecalho.justificativa, columnIndex, ascending),
                        ),
                      ],
                      source: _estoqueReajusteCabecalhoDataSource,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
  }

  void _inserir() {
    Navigator.of(context)
      .push(MaterialPageRoute(
        builder: (BuildContext context) =>
          EstoqueReajusteCabecalhoPage(estoqueReajusteCabecalho: EstoqueReajusteCabecalho(), title: 'Estoque Reajuste Cabecalho - Inserindo', operacao: 'I')))
      .then((_) {
        Provider.of<EstoqueReajusteCabecalhoViewModel>(context, listen: false).consultarLista();
    });
  }

  void _chamarFiltro() async {
    _filtro = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => FiltroPage(
            title: 'Estoque Reajuste Cabecalho - Filtro',
            colunas: _colunas,
            filtroPadrao: true,
          ),
          fullscreenDialog: true,
        ));
    if (_filtro != null) {
      if (_filtro.campo != null) {
        _filtro.campo = _campos[int.parse(_filtro.campo)];
        await Provider.of<EstoqueReajusteCabecalhoViewModel>(context, listen: false).consultarLista(filtro: _filtro);
      }
    }    
  }

  Future _gerarRelatorio() async {
    gerarDialogBoxConfirmacao(
      context, Constantes.perguntaGerarRelatorio, () async {
      Navigator.of(context).pop();

      if (kIsWeb) {
	    await Provider.of<EstoqueReajusteCabecalhoViewModel>(context).visualizarPdfWeb(filtro: _filtro);
	  } else {
        var arquivoPdf = await Provider.of<EstoqueReajusteCabecalhoViewModel>(context).visualizarPdf(filtro: _filtro);
        Navigator.of(context)
          .push(MaterialPageRoute(
		    builder: (BuildContext context) => PdfPage(arquivoPdf: arquivoPdf, title: 'Relatório - Estoque Reajuste Cabecalho')));
      }
    });
  }

  Future<Null> _refrescarTela() async {
    await Provider.of<EstoqueReajusteCabecalhoViewModel>(context, listen: false).consultarLista();
    return null;
  }
}

/// codigo referente a fonte de dados
class _EstoqueReajusteCabecalhoDataSource extends DataTableSource {
  final List<EstoqueReajusteCabecalho> listaEstoqueReajusteCabecalho;
  final BuildContext context;

  _EstoqueReajusteCabecalhoDataSource(this.listaEstoqueReajusteCabecalho, this.context);

  void _sort<T>(Comparable<T> getField(EstoqueReajusteCabecalho estoqueReajusteCabecalho), bool ascending) {
    listaEstoqueReajusteCabecalho.sort((EstoqueReajusteCabecalho a, EstoqueReajusteCabecalho b) {
      if (!ascending) {
        final EstoqueReajusteCabecalho c = a;
        a = b;
        b = c;
      }
      Comparable<T> aValue = getField(a);
      Comparable<T> bValue = getField(b);

      if (aValue == null) aValue = '' as Comparable<T>;
      if (bValue == null) bValue = '' as Comparable<T>;

      return Comparable.compare(aValue, bValue);
    });
    notifyListeners();
  }

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= listaEstoqueReajusteCabecalho.length) return null;
    final EstoqueReajusteCabecalho estoqueReajusteCabecalho = listaEstoqueReajusteCabecalho[index];
    return DataRow.byIndex(
      index: index,
      cells: <DataCell>[
        DataCell(Text('${estoqueReajusteCabecalho.id ?? ''}'), onTap: () {
          _detalharEstoqueReajusteCabecalho(estoqueReajusteCabecalho, context);
        }),
        DataCell(Text('${estoqueReajusteCabecalho.colaborador?.pessoa?.nome ?? ''}'), onTap: () {
          _detalharEstoqueReajusteCabecalho(estoqueReajusteCabecalho, context);
        }),
        DataCell(Text('${estoqueReajusteCabecalho.dataReajuste != null ? DateFormat('dd/MM/yyyy').format(estoqueReajusteCabecalho.dataReajuste) : ''}'), onTap: () {
          _detalharEstoqueReajusteCabecalho(estoqueReajusteCabecalho, context);
        }),
        DataCell(Text('${Constantes.formatoDecimalTaxa.format(estoqueReajusteCabecalho.taxa ?? 0)}'), onTap: () {
          _detalharEstoqueReajusteCabecalho(estoqueReajusteCabecalho, context);
        }),
        DataCell(Text('${estoqueReajusteCabecalho.tipoReajuste ?? ''}'), onTap: () {
          _detalharEstoqueReajusteCabecalho(estoqueReajusteCabecalho, context);
        }),
        DataCell(Text('${estoqueReajusteCabecalho.justificativa ?? ''}'), onTap: () {
          _detalharEstoqueReajusteCabecalho(estoqueReajusteCabecalho, context);
        }),
      ],
    );
  }

  @override
  int get rowCount => listaEstoqueReajusteCabecalho.length ?? 0;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}

void _detalharEstoqueReajusteCabecalho(EstoqueReajusteCabecalho estoqueReajusteCabecalho, BuildContext context) {
  Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) =>
        EstoqueReajusteCabecalhoPage(estoqueReajusteCabecalho: estoqueReajusteCabecalho, title: 'Estoque Reajuste Cabecalho - Editando', operacao: 'A')));
}