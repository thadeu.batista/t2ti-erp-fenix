/*
Title: T2Ti ERP 3.0                                                                
Description: ListaPage relacionada à tabela [FIN_DOCUMENTO_ORIGEM] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2021 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
import 'package:fenix/src/infra/sessao.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart';

import 'package:fenix/src/infra/constantes.dart';

import 'package:fenix/src/view/shared/view_util_lib.dart';
import 'package:fenix/src/infra/atalhos_desktop_web.dart';
import 'package:fenix/src/view/shared/botoes.dart';
import 'package:fenix/src/view/shared/caixas_de_dialogo.dart';

import 'package:fenix/src/model/model.dart';
import 'package:fenix/src/model/filtro.dart';
import 'package:fenix/src/view_model/view_model.dart';

import 'package:fenix/src/view/shared/page/filtro_page.dart';
import 'package:fenix/src/view/shared/page/pdf_page.dart';

import 'fin_documento_origem_persiste_page.dart';

class FinDocumentoOrigemListaPage extends StatefulWidget {
  @override
  _FinDocumentoOrigemListaPageState createState() => _FinDocumentoOrigemListaPageState();
}

class _FinDocumentoOrigemListaPageState extends State<FinDocumentoOrigemListaPage> {
  int _rowsPerPage = Constantes.paginatedDataTableLinhasPorPagina;
  int _sortColumnIndex;
  bool _sortAscending = true;
  var _filtro = Filtro();
  var _colunas = FinDocumentoOrigem.colunas;
  var _campos = FinDocumentoOrigem.campos;

  Map<LogicalKeySet, Intent> _shortcutMap; 
  Map<Type, Action<Intent>> _actionMap;

  @override
  void didChangeDependencies() {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => Sessao.tratarErrosSessao(context, Provider.of<FinDocumentoOrigemViewModel>(context, listen: false).objetoJsonErro)
    );
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _shortcutMap = getAtalhosListaPage();

    _actionMap = <Type, Action<Intent>>{
      AtalhoTelaIntent: CallbackAction<AtalhoTelaIntent>(
        onInvoke: _tratarAcoesAtalhos,
      ),
    };
  }

  void _tratarAcoesAtalhos(AtalhoTelaIntent intent) {
    switch (intent.type) {
      case AtalhoTelaType.inserir:
        _inserir();
        break;
      case AtalhoTelaType.imprimir:
        _gerarRelatorio();
        break;
      case AtalhoTelaType.filtrar:
        _chamarFiltro();
        break;
      default:
        break;
    }
  }
  
  @override
  Widget build(BuildContext context) {
    if (Provider.of<FinDocumentoOrigemViewModel>(context).listaFinDocumentoOrigem == null && Provider.of<FinDocumentoOrigemViewModel>(context).objetoJsonErro == null) {
      Provider.of<FinDocumentoOrigemViewModel>(context, listen: false).consultarLista();
    }
    var _listaFinDocumentoOrigem = Provider.of<FinDocumentoOrigemViewModel>(context).listaFinDocumentoOrigem;

    final _FinDocumentoOrigemDataSource _finDocumentoOrigemDataSource = _FinDocumentoOrigemDataSource(_listaFinDocumentoOrigem, context);

    void _sort<T>(Comparable<T> getField(FinDocumentoOrigem finDocumentoOrigem), int columnIndex, bool ascending) {
      _finDocumentoOrigemDataSource._sort<T>(getField, ascending);
      setState(() {
        _sortColumnIndex = columnIndex;
        _sortAscending = ascending;
      });
    }
	
      return FocusableActionDetector(
        actions: _actionMap,
        shortcuts: _shortcutMap,
        child: Focus(
          autofocus: true,
          child: Scaffold(
            appBar: AppBar(
              title: const Text('Cadastro - Fin Documento Origem'),
              actions: <Widget>[],
            ),
            floatingActionButton: FloatingActionButton(
              focusColor: ViewUtilLib.getBotaoFocusColor(),
              tooltip: Constantes.botaoInserirDica,
              backgroundColor: ViewUtilLib.getBackgroundColorBotaoInserir(),
              child: ViewUtilLib.getIconBotaoInserir(),
              onPressed: () {
                _inserir();
              }),
            floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
            bottomNavigationBar: BottomAppBar(
              color: ViewUtilLib.getBottomAppBarColor(),          
              shape: CircularNotchedRectangle(),
              child: Row(
                children: getBotoesNavigationBarListaPage(
                  context: context, 
                  chamarFiltro: _chamarFiltro, 
                  gerarRelatorio: _gerarRelatorio,
                ),
              ),
            ),
            body: RefreshIndicator(
              onRefresh: _refrescarTela,
              child: Scrollbar(
                child: _listaFinDocumentoOrigem == null
                ? Center(child: CircularProgressIndicator())
                : ListView(
                  padding: EdgeInsets.all(Constantes.paddingListViewListaPage),
                  children: <Widget>[
                    PaginatedDataTable(                        
                      header: const Text('Relação - Fin Documento Origem'),
                      rowsPerPage: _rowsPerPage,
                      onRowsPerPageChanged: (int value) {
                        setState(() {
                          _rowsPerPage = value;
                        });
                      },
                      sortColumnIndex: _sortColumnIndex,
                      sortAscending: _sortAscending,
                      columns: <DataColumn>[
                        DataColumn(
                          numeric: true,
                          label: const Text('Id'),
                          tooltip: 'Id',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((FinDocumentoOrigem finDocumentoOrigem) => finDocumentoOrigem.id, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Código'),
                          tooltip: 'Código',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((FinDocumentoOrigem finDocumentoOrigem) => finDocumentoOrigem.codigo, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Sigla'),
                          tooltip: 'Sigla',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((FinDocumentoOrigem finDocumentoOrigem) => finDocumentoOrigem.sigla, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Descrição'),
                          tooltip: 'Descrição',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((FinDocumentoOrigem finDocumentoOrigem) => finDocumentoOrigem.descricao, columnIndex, ascending),
                        ),
                      ],
                      source: _finDocumentoOrigemDataSource,
                    ),
                  ],
                ),
              ),
            ),          
          ),
        ),
      );
    
  }

  void _inserir() {
    Navigator.of(context)
      .push(MaterialPageRoute(
        builder: (BuildContext context) => 
            FinDocumentoOrigemPersistePage(finDocumentoOrigem: FinDocumentoOrigem(), title: 'Fin Documento Origem - Inserindo', operacao: 'I')))
      .then((_) {
        Provider.of<FinDocumentoOrigemViewModel>(context, listen: false).consultarLista();
    });
  }

  void _chamarFiltro() async {
    _filtro = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => FiltroPage(
            title: 'Fin Documento Origem - Filtro',
            colunas: _colunas,
            filtroPadrao: true,
          ),
          fullscreenDialog: true,
        ));
    if (_filtro != null) {
      if (_filtro.campo != null) {
        _filtro.campo = _campos[int.parse(_filtro.campo)];
        await Provider.of<FinDocumentoOrigemViewModel>(context, listen: false).consultarLista(filtro: _filtro);
      }
    }    
  }

  Future _gerarRelatorio() async {
    gerarDialogBoxConfirmacao(
      context, Constantes.perguntaGerarRelatorio, () async {
      Navigator.of(context).pop();

      if (kIsWeb) {
        await Provider.of<FinDocumentoOrigemViewModel>(context).visualizarPdfWeb(filtro: _filtro);
      } else {
        var arquivoPdf = await Provider.of<FinDocumentoOrigemViewModel>(context).visualizarPdf(filtro: _filtro);
        Navigator.of(context)
          .push(MaterialPageRoute(
            builder: (BuildContext context) => PdfPage(
              arquivoPdf: arquivoPdf, title: 'Relatório - Fin Documento Origem')));
      }
    });
  }

  Future<Null> _refrescarTela() async {
    await Provider.of<FinDocumentoOrigemViewModel>(context, listen: false).consultarLista();
    return null;
  }
}

/// codigo referente a fonte de dados
class _FinDocumentoOrigemDataSource extends DataTableSource {
  final List<FinDocumentoOrigem> listaFinDocumentoOrigem;
  final BuildContext context;

  _FinDocumentoOrigemDataSource(this.listaFinDocumentoOrigem, this.context);

  void _sort<T>(Comparable<T> getField(FinDocumentoOrigem finDocumentoOrigem), bool ascending) {
    listaFinDocumentoOrigem.sort((FinDocumentoOrigem a, FinDocumentoOrigem b) {
      if (!ascending) {
        final FinDocumentoOrigem c = a;
        a = b;
        b = c;
      }
      Comparable<T> aValue = getField(a);
      Comparable<T> bValue = getField(b);

      if (aValue == null) aValue = '' as Comparable<T>;
      if (bValue == null) bValue = '' as Comparable<T>;

      return Comparable.compare(aValue, bValue);
    });
    notifyListeners();
  }

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= listaFinDocumentoOrigem.length) return null;
    final FinDocumentoOrigem finDocumentoOrigem = listaFinDocumentoOrigem[index];
    return DataRow.byIndex(
      index: index,
      cells: <DataCell>[
        DataCell(Text('${finDocumentoOrigem.id ?? ''}'), onTap: () {
          _detalharFinDocumentoOrigem(finDocumentoOrigem, context);
        }),
        DataCell(Text('${finDocumentoOrigem.codigo ?? ''}'), onTap: () {
          _detalharFinDocumentoOrigem(finDocumentoOrigem, context);
        }),
        DataCell(Text('${finDocumentoOrigem.sigla ?? ''}'), onTap: () {
          _detalharFinDocumentoOrigem(finDocumentoOrigem, context);
        }),
        DataCell(Text('${finDocumentoOrigem.descricao ?? ''}'), onTap: () {
          _detalharFinDocumentoOrigem(finDocumentoOrigem, context);
        }),
      ],
    );
  }

  @override
  int get rowCount => listaFinDocumentoOrigem.length ?? 0;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}

void _detalharFinDocumentoOrigem(FinDocumentoOrigem finDocumentoOrigem, BuildContext context) {
   Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => FinDocumentoOrigemPersistePage(
          finDocumentoOrigem: finDocumentoOrigem,
          title: 'Fin Documento Origem - Editando',
          operacao: 'A')));

  /*Navigator.pushNamed(
    context,
    '/finDocumentoOrigemDetalhe',
    arguments: finDocumentoOrigem,
  );*/
}