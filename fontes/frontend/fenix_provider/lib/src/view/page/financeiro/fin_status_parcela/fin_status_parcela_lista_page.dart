/*
Title: T2Ti ERP 3.0                                                                
Description: ListaPage relacionada à tabela [FIN_STATUS_PARCELA] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2021 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
import 'package:fenix/src/infra/sessao.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart';

import 'package:fenix/src/infra/constantes.dart';

import 'package:fenix/src/view/shared/view_util_lib.dart';
import 'package:fenix/src/infra/atalhos_desktop_web.dart';
import 'package:fenix/src/view/shared/botoes.dart';
import 'package:fenix/src/view/shared/caixas_de_dialogo.dart';

import 'package:fenix/src/model/model.dart';
import 'package:fenix/src/model/filtro.dart';
import 'package:fenix/src/view_model/view_model.dart';

import 'package:fenix/src/view/shared/page/filtro_page.dart';
import 'package:fenix/src/view/shared/page/pdf_page.dart';

import 'fin_status_parcela_persiste_page.dart';

class FinStatusParcelaListaPage extends StatefulWidget {
  @override
  _FinStatusParcelaListaPageState createState() => _FinStatusParcelaListaPageState();
}

class _FinStatusParcelaListaPageState extends State<FinStatusParcelaListaPage> {
  int _rowsPerPage = Constantes.paginatedDataTableLinhasPorPagina;
  int _sortColumnIndex;
  bool _sortAscending = true;
  var _filtro = Filtro();
  var _colunas = FinStatusParcela.colunas;
  var _campos = FinStatusParcela.campos;

  Map<LogicalKeySet, Intent> _shortcutMap; 
  Map<Type, Action<Intent>> _actionMap;

  @override
  void didChangeDependencies() {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => Sessao.tratarErrosSessao(context, Provider.of<FinStatusParcelaViewModel>(context, listen: false).objetoJsonErro)
    );
    super.didChangeDependencies();
  }


  @override
  void initState() {
    super.initState();
    _shortcutMap = getAtalhosListaPage();

    _actionMap = <Type, Action<Intent>>{
      AtalhoTelaIntent: CallbackAction<AtalhoTelaIntent>(
        onInvoke: _tratarAcoesAtalhos,
      ),
    };
  }

  void _tratarAcoesAtalhos(AtalhoTelaIntent intent) {
    switch (intent.type) {
      case AtalhoTelaType.inserir:
        _inserir();
        break;
      case AtalhoTelaType.imprimir:
        _gerarRelatorio();
        break;
      case AtalhoTelaType.filtrar:
        _chamarFiltro();
        break;
      default:
        break;
    }
  }
  
  @override
  Widget build(BuildContext context) {
    if (Provider.of<FinStatusParcelaViewModel>(context).listaFinStatusParcela == null && Provider.of<FinStatusParcelaViewModel>(context).objetoJsonErro == null) {
      Provider.of<FinStatusParcelaViewModel>(context, listen: false).consultarLista();
    }
    var _listaFinStatusParcela = Provider.of<FinStatusParcelaViewModel>(context).listaFinStatusParcela;

    final _FinStatusParcelaDataSource _finStatusParcelaDataSource = _FinStatusParcelaDataSource(_listaFinStatusParcela, context);

    void _sort<T>(Comparable<T> getField(FinStatusParcela finStatusParcela), int columnIndex, bool ascending) {
      _finStatusParcelaDataSource._sort<T>(getField, ascending);
      setState(() {
        _sortColumnIndex = columnIndex;
        _sortAscending = ascending;
      });
    }
	
      return FocusableActionDetector(
        actions: _actionMap,
        shortcuts: _shortcutMap,
        child: Focus(
          autofocus: true,
          child: Scaffold(
            appBar: AppBar(
              title: const Text('Cadastro - Fin Status Parcela'),
              actions: <Widget>[],
            ),
            floatingActionButton: FloatingActionButton(
              focusColor: ViewUtilLib.getBotaoFocusColor(),
              tooltip: Constantes.botaoInserirDica,
              backgroundColor: ViewUtilLib.getBackgroundColorBotaoInserir(),
              child: ViewUtilLib.getIconBotaoInserir(),
              onPressed: () {
                _inserir();
              }),
            floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
            bottomNavigationBar: BottomAppBar(
              color: ViewUtilLib.getBottomAppBarColor(),          
              shape: CircularNotchedRectangle(),
              child: Row(
                children: getBotoesNavigationBarListaPage(
                  context: context, 
                  chamarFiltro: _chamarFiltro, 
                  gerarRelatorio: _gerarRelatorio,
                ),
              ),
            ),
            body: RefreshIndicator(
              onRefresh: _refrescarTela,
              child: Scrollbar(
                child: _listaFinStatusParcela == null
                ? Center(child: CircularProgressIndicator())
                : ListView(
                  padding: EdgeInsets.all(Constantes.paddingListViewListaPage),
                  children: <Widget>[
                    PaginatedDataTable(                        
                      header: const Text('Relação - Fin Status Parcela'),
                      rowsPerPage: _rowsPerPage,
                      onRowsPerPageChanged: (int value) {
                        setState(() {
                          _rowsPerPage = value;
                        });
                      },
                      sortColumnIndex: _sortColumnIndex,
                      sortAscending: _sortAscending,
                      columns: <DataColumn>[
                        DataColumn(
                          numeric: true,
                          label: const Text('Id'),
                          tooltip: 'Id',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((FinStatusParcela finStatusParcela) => finStatusParcela.id, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Situação'),
                          tooltip: 'Situação',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((FinStatusParcela finStatusParcela) => finStatusParcela.situacao, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Descrição'),
                          tooltip: 'Descrição',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((FinStatusParcela finStatusParcela) => finStatusParcela.descricao, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Procedimento'),
                          tooltip: 'Procedimento',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((FinStatusParcela finStatusParcela) => finStatusParcela.procedimento, columnIndex, ascending),
                        ),
                      ],
                      source: _finStatusParcelaDataSource,
                    ),
                  ],
                ),
              ),
            ),          
          ),
        ),
      );
    
  }

  void _inserir() {
    Navigator.of(context)
      .push(MaterialPageRoute(
        builder: (BuildContext context) => 
            FinStatusParcelaPersistePage(finStatusParcela: FinStatusParcela(), title: 'Fin Status Parcela - Inserindo', operacao: 'I')))
      .then((_) {
        Provider.of<FinStatusParcelaViewModel>(context, listen: false).consultarLista();
    });
  }

  void _chamarFiltro() async {
    _filtro = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => FiltroPage(
            title: 'Fin Status Parcela - Filtro',
            colunas: _colunas,
            filtroPadrao: true,
          ),
          fullscreenDialog: true,
        ));
    if (_filtro != null) {
      if (_filtro.campo != null) {
        _filtro.campo = _campos[int.parse(_filtro.campo)];
        await Provider.of<FinStatusParcelaViewModel>(context, listen: false).consultarLista(filtro: _filtro);
      }
    }    
  }

  Future _gerarRelatorio() async {
    gerarDialogBoxConfirmacao(
      context, Constantes.perguntaGerarRelatorio, () async {
      Navigator.of(context).pop();

      if (kIsWeb) {
        await Provider.of<FinStatusParcelaViewModel>(context).visualizarPdfWeb(filtro: _filtro);
      } else {
        var arquivoPdf = await Provider.of<FinStatusParcelaViewModel>(context).visualizarPdf(filtro: _filtro);
        Navigator.of(context)
          .push(MaterialPageRoute(
            builder: (BuildContext context) => PdfPage(
              arquivoPdf: arquivoPdf, title: 'Relatório - Fin Status Parcela')));
      }
    });
  }

  Future<Null> _refrescarTela() async {
    await Provider.of<FinStatusParcelaViewModel>(context, listen: false).consultarLista();
    return null;
  }
}

/// codigo referente a fonte de dados
class _FinStatusParcelaDataSource extends DataTableSource {
  final List<FinStatusParcela> listaFinStatusParcela;
  final BuildContext context;

  _FinStatusParcelaDataSource(this.listaFinStatusParcela, this.context);

  void _sort<T>(Comparable<T> getField(FinStatusParcela finStatusParcela), bool ascending) {
    listaFinStatusParcela.sort((FinStatusParcela a, FinStatusParcela b) {
      if (!ascending) {
        final FinStatusParcela c = a;
        a = b;
        b = c;
      }
      Comparable<T> aValue = getField(a);
      Comparable<T> bValue = getField(b);

      if (aValue == null) aValue = '' as Comparable<T>;
      if (bValue == null) bValue = '' as Comparable<T>;

      return Comparable.compare(aValue, bValue);
    });
    notifyListeners();
  }

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= listaFinStatusParcela.length) return null;
    final FinStatusParcela finStatusParcela = listaFinStatusParcela[index];
    return DataRow.byIndex(
      index: index,
      cells: <DataCell>[
        DataCell(Text('${finStatusParcela.id ?? ''}'), onTap: () {
          _detalharFinStatusParcela(finStatusParcela, context);
        }),
        DataCell(Text('${finStatusParcela.situacao ?? ''}'), onTap: () {
          _detalharFinStatusParcela(finStatusParcela, context);
        }),
        DataCell(Text('${finStatusParcela.descricao ?? ''}'), onTap: () {
          _detalharFinStatusParcela(finStatusParcela, context);
        }),
        DataCell(Text('${finStatusParcela.procedimento ?? ''}'), onTap: () {
          _detalharFinStatusParcela(finStatusParcela, context);
        }),
      ],
    );
  }

  @override
  int get rowCount => listaFinStatusParcela.length ?? 0;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}

void _detalharFinStatusParcela(FinStatusParcela finStatusParcela, BuildContext context) {
   Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => FinStatusParcelaPersistePage(
          finStatusParcela: finStatusParcela,
          title: 'Fin Status Parcela - Editando',
          operacao: 'A')));

  /*Navigator.pushNamed(
    context,
    '/finStatusParcelaDetalhe',
    arguments: finStatusParcela,
  );*/
}