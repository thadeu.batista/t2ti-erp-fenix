/*
Title: T2Ti ERP 3.0                                                                
Description: ListaPage relacionada à tabela [VENDA_FRETE] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2021 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart';

import 'package:fenix/src/infra/sessao.dart';
import 'package:fenix/src/infra/constantes.dart';

import 'package:fenix/src/view/shared/view_util_lib.dart';
import 'package:fenix/src/view/shared/botoes.dart';
import 'package:fenix/src/infra/atalhos_desktop_web.dart';
import 'package:fenix/src/view/shared/caixas_de_dialogo.dart';

import 'package:fenix/src/model/model.dart';
import 'package:fenix/src/model/filtro.dart';
import 'package:fenix/src/view_model/view_model.dart';

import 'package:fenix/src/view/shared/page/filtro_page.dart';
import 'package:fenix/src/view/shared/page/pdf_page.dart';

import 'venda_frete_persiste_page.dart';

class VendaFreteListaPage extends StatefulWidget {
  @override
  _VendaFreteListaPageState createState() => _VendaFreteListaPageState();
}

class _VendaFreteListaPageState extends State<VendaFreteListaPage> {
  int _rowsPerPage = Constantes.paginatedDataTableLinhasPorPagina;
  int _sortColumnIndex;
  bool _sortAscending = true;
  var _filtro = Filtro();
  final _colunas = VendaFrete.colunas;
  final _campos = VendaFrete.campos;

  Map<LogicalKeySet, Intent> _shortcutMap; 
  Map<Type, Action<Intent>> _actionMap;

  @override
  void initState() {
    super.initState();
    _shortcutMap = getAtalhosListaPage();

    _actionMap = <Type, Action<Intent>>{
      AtalhoTelaIntent: CallbackAction<AtalhoTelaIntent>(
        onInvoke: _tratarAcoesAtalhos,
      ),
    };
  }

  @override
  void didChangeDependencies() {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => Sessao.tratarErrosSessao(context, Provider.of<VendaFreteViewModel>(context, listen: false).objetoJsonErro)
    );
    super.didChangeDependencies();
  }
  
  void _tratarAcoesAtalhos(AtalhoTelaIntent intent) {
    switch (intent.type) {
      case AtalhoTelaType.inserir:
        _inserir();
        break;
      case AtalhoTelaType.imprimir:
        _gerarRelatorio();
        break;
      case AtalhoTelaType.filtrar:
        _chamarFiltro();
        break;
      default:
        break;
    }
  }
  
  @override
  Widget build(BuildContext context) {
    final _listaVendaFrete = Provider.of<VendaFreteViewModel>(context).listaVendaFrete;
    final _objetoJsonErro = Provider.of<VendaFreteViewModel>(context).objetoJsonErro;

    if (_listaVendaFrete == null && _objetoJsonErro == null) {
      Provider.of<VendaFreteViewModel>(context, listen: false).consultarLista();
    }
  
    final _VendaFreteDataSource _vendaFreteDataSource = _VendaFreteDataSource(_listaVendaFrete, context);

    void _sort<T>(Comparable<T> getField(VendaFrete vendaFrete), int columnIndex, bool ascending) {
      _vendaFreteDataSource._sort<T>(getField, ascending);
      setState(() {
        _sortColumnIndex = columnIndex;
        _sortAscending = ascending;
      });
    }
	
      return FocusableActionDetector(
        actions: _actionMap,
        shortcuts: _shortcutMap,
        child: Focus(
          autofocus: true,
          child: Scaffold(
            appBar: AppBar(
              title: const Text('Cadastro - Venda Frete'),
              actions: <Widget>[],
            ),
            floatingActionButton: FloatingActionButton(
              focusColor: ViewUtilLib.getBotaoFocusColor(),
              tooltip: Constantes.botaoInserirDica,
              backgroundColor: ViewUtilLib.getBackgroundColorBotaoInserir(),
              child: ViewUtilLib.getIconBotaoInserir(),
              onPressed: () {
                _inserir();
              }),
            floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
            bottomNavigationBar: BottomAppBar(
              color: ViewUtilLib.getBottomAppBarColor(),          
              shape: CircularNotchedRectangle(),
              child: Row(
                children: getBotoesNavigationBarListaPage(
                  context: context, 
                  chamarFiltro: _chamarFiltro, 
                  gerarRelatorio: _gerarRelatorio,
                ),
              ),
            ),
            body: RefreshIndicator(
              onRefresh: _refrescarTela,
              child: Scrollbar(
                child: _listaVendaFrete == null
                ? Center(child: CircularProgressIndicator())
                : ListView(
                  padding: EdgeInsets.all(Constantes.paddingListViewListaPage),
                  children: <Widget>[
                    PaginatedDataTable(                        
                      header: const Text('Relação - Venda Frete'),
                      rowsPerPage: _rowsPerPage,
                      onRowsPerPageChanged: (int value) {
                        setState(() {
                          _rowsPerPage = value;
                        });
                      },
                      sortColumnIndex: _sortColumnIndex,
                      sortAscending: _sortAscending,
                      columns: <DataColumn>[
                        DataColumn(
                          numeric: true,
                          label: const Text('Id'),
                          tooltip: 'Id',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((VendaFrete vendaFrete) => vendaFrete.id, columnIndex, ascending),
                        ),
                        DataColumn(
                          numeric: true,
                          label: const Text('Venda'),
                          tooltip: 'Venda',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((VendaFrete vendaFrete) => vendaFrete.vendaCabecalho?.id, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Transportadora'),
                          tooltip: 'Transportadora',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((VendaFrete vendaFrete) => vendaFrete.transportadora?.pessoa?.nome, columnIndex, ascending),
                        ),
                        DataColumn(
                          numeric: true,
                          label: const Text('Conhecimento'),
                          tooltip: 'Conhecimento',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((VendaFrete vendaFrete) => vendaFrete.conhecimento, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Responsável'),
                          tooltip: 'Responsável',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((VendaFrete vendaFrete) => vendaFrete.responsavel, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Placa'),
                          tooltip: 'Placa',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((VendaFrete vendaFrete) => vendaFrete.placa, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('UF'),
                          tooltip: 'UF',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((VendaFrete vendaFrete) => vendaFrete.ufPlaca, columnIndex, ascending),
                        ),
                        DataColumn(
                          numeric: true,
                          label: const Text('Selo Fiscal'),
                          tooltip: 'Selo Fiscal',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((VendaFrete vendaFrete) => vendaFrete.seloFiscal, columnIndex, ascending),
                        ),
                        DataColumn(
                          numeric: true,
                          label: const Text('Quantidade de Volumes'),
                          tooltip: 'Quantidade de Volumes',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((VendaFrete vendaFrete) => vendaFrete.quantidadeVolume, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Marca Volume'),
                          tooltip: 'Marca Volume',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((VendaFrete vendaFrete) => vendaFrete.marcaVolume, columnIndex, ascending),
                        ),
                        DataColumn(
                          label: const Text('Espécie do Volume'),
                          tooltip: 'Espécie do Volume',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<String>((VendaFrete vendaFrete) => vendaFrete.especieVolume, columnIndex, ascending),
                        ),
                        DataColumn(
                          numeric: true,
                          label: const Text('Peso Bruto'),
                          tooltip: 'Peso Bruto',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((VendaFrete vendaFrete) => vendaFrete.pesoBruto, columnIndex, ascending),
                        ),
                        DataColumn(
                          numeric: true,
                          label: const Text('Peso Líquido'),
                          tooltip: 'Peso Líquido',
                          onSort: (int columnIndex, bool ascending) =>
                            _sort<num>((VendaFrete vendaFrete) => vendaFrete.pesoLiquido, columnIndex, ascending),
                        ),
                      ],
                      source: _vendaFreteDataSource,
                    ),
                  ],
                ),
              ),
            ),          
          ),
        ),
      );
  }

  void _inserir() {
    Navigator.of(context)
      .push(MaterialPageRoute(
        builder: (BuildContext context) => 
          VendaFretePersistePage(vendaFrete: VendaFrete(), title: 'Venda Frete - Inserindo', operacao: 'I')))
      .then((_) {
        Provider.of<VendaFreteViewModel>(context, listen: false).consultarLista();
    });
  }

  void _chamarFiltro() async {
    _filtro = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => FiltroPage(
            title: 'Venda Frete - Filtro',
            colunas: _colunas,
            filtroPadrao: true,
          ),
          fullscreenDialog: true,
        ));
    if (_filtro != null) {
      if (_filtro.campo != null) {
        _filtro.campo = _campos[int.parse(_filtro.campo)];
        await Provider.of<VendaFreteViewModel>(context, listen: false).consultarLista(filtro: _filtro);
      }
    }    
  }

  Future _gerarRelatorio() async {
    gerarDialogBoxConfirmacao(
      context, Constantes.perguntaGerarRelatorio, () async {
      Navigator.of(context).pop();

      if (kIsWeb) {
        await Provider.of<VendaFreteViewModel>(context).visualizarPdfWeb(filtro: _filtro);
      } else {
        var arquivoPdf = await Provider.of<VendaFreteViewModel>(context).visualizarPdf(filtro: _filtro);
        Navigator.of(context)
          .push(MaterialPageRoute(
            builder: (BuildContext context) => PdfPage(arquivoPdf: arquivoPdf, title: 'Relatório - Venda Frete')));
      }
    });
  }

  Future<Null> _refrescarTela() async {
    await Provider.of<VendaFreteViewModel>(context, listen: false).consultarLista();
    return null;
  }
}

/// codigo referente a fonte de dados
class _VendaFreteDataSource extends DataTableSource {
  final List<VendaFrete> listaVendaFrete;
  final BuildContext context;

  _VendaFreteDataSource(this.listaVendaFrete, this.context);

  void _sort<T>(Comparable<T> getField(VendaFrete vendaFrete), bool ascending) {
    listaVendaFrete.sort((VendaFrete a, VendaFrete b) {
      if (!ascending) {
        final VendaFrete c = a;
        a = b;
        b = c;
      }
      Comparable<T> aValue = getField(a);
      Comparable<T> bValue = getField(b);

      if (aValue == null) aValue = '' as Comparable<T>;
      if (bValue == null) bValue = '' as Comparable<T>;

      return Comparable.compare(aValue, bValue);
    });
    notifyListeners();
  }

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= listaVendaFrete.length) return null;
    final VendaFrete vendaFrete = listaVendaFrete[index];
    return DataRow.byIndex(
      index: index,
      cells: <DataCell>[
        DataCell(Text('${vendaFrete.id ?? ''}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${vendaFrete.vendaCabecalho?.id ?? ''}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${vendaFrete.transportadora?.pessoa?.nome ?? ''}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${vendaFrete.conhecimento ?? ''}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${vendaFrete.responsavel ?? ''}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${vendaFrete.placa ?? ''}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${vendaFrete.ufPlaca ?? ''}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${vendaFrete.seloFiscal ?? ''}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${Constantes.formatoDecimalQuantidade.format(vendaFrete.quantidadeVolume ?? 0)}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${vendaFrete.marcaVolume ?? ''}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${vendaFrete.especieVolume ?? ''}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${Constantes.formatoDecimalQuantidade.format(vendaFrete.pesoBruto ?? 0)}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
        DataCell(Text('${Constantes.formatoDecimalQuantidade.format(vendaFrete.pesoLiquido ?? 0)}'), onTap: () {
          _detalharVendaFrete(vendaFrete, context);
        }),
      ],
    );
  }

  @override
  int get rowCount => listaVendaFrete.length ?? 0;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}

void _detalharVendaFrete(VendaFrete vendaFrete, BuildContext context) {
  Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => 
        VendaFretePersistePage(vendaFrete: vendaFrete, title: 'Venda Frete - Editando', operacao: 'A')));
}