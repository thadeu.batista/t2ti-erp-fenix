/*
Title: T2Ti ERP 3.0                                                                
Description: Model relacionado à tabela [NFE_CABECALHO] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2021 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
import 'dart:convert';

import 'package:fenix/src/model/views_db/view_sped_nfe_detalhe.dart';
import 'package:intl/intl.dart';
import 'package:fenix/src/model/model.dart';

import 'nfe_acesso_xml.dart';
import 'nfe_cana.dart';
import 'nfe_cte_referenciado.dart';
import 'nfe_cupom_fiscal_referenciado.dart';
import 'nfe_destinatario.dart';
import 'nfe_detalhe.dart';
import 'nfe_emitente.dart';
import 'nfe_fatura.dart';
import 'nfe_informacao_pagamento.dart';
import 'nfe_local_entrega.dart';
import 'nfe_local_retirada.dart';
import 'nfe_nf_referenciada.dart';
import 'nfe_processo_referenciado.dart';
import 'nfe_prod_rural_referenciada.dart';
import 'nfe_referenciada.dart';
import 'nfe_responsavel_tecnico.dart';
import 'nfe_transporte.dart';

class NfeCabecalho {
	int id;
	int idVendedor;
	int idFornecedor;
	int idCliente;
	int idTributOperacaoFiscal;
	int idVendaCabecalho;
	int idNfceMovimento;
	int ufEmitente;
	String codigoNumerico;
	String naturezaOperacao;
	String codigoModelo;
	String serie;
	String numero;
	DateTime dataHoraEmissao;
	DateTime dataHoraEntradaSaida;
	String tipoOperacao;
	String localDestino;
	int codigoMunicipio;
	String formatoImpressaoDanfe;
	String tipoEmissao;
	String chaveAcesso;
	String digitoChaveAcesso;
	String ambiente;
	String finalidadeEmissao;
	String consumidorOperacao;
	String consumidorPresenca;
	String processoEmissao;
	String versaoProcessoEmissao;
	DateTime dataEntradaContingencia;
	String justificativaContingencia;
	double baseCalculoIcms;
	double valorIcms;
	double valorIcmsDesonerado;
	double totalIcmsFcpUfDestino;
	double totalIcmsInterestadualUfDestino;
	double totalIcmsInterestadualUfRemente;
	double valorTotalFcp;
	double baseCalculoIcmsSt;
	double valorIcmsSt;
	double valorTotalFcpSt;
	double valorTotalFcpStRetido;
	double valorTotalProdutos;
	double valorFrete;
	double valorSeguro;
	double valorDesconto;
	double valorImpostoImportacao;
	double valorIpi;
	double valorIpiDevolvido;
	double valorPis;
	double valorCofins;
	double valorDespesasAcessorias;
	double valorTotal;
	double valorTotalTributos;
	double valorServicos;
	double baseCalculoIssqn;
	double valorIssqn;
	double valorPisIssqn;
	double valorCofinsIssqn;
	DateTime dataPrestacaoServico;
	double valorDeducaoIssqn;
	double outrasRetencoesIssqn;
	double descontoIncondicionadoIssqn;
	double descontoCondicionadoIssqn;
	double totalRetencaoIssqn;
	String regimeEspecialTributacao;
	double valorRetidoPis;
	double valorRetidoCofins;
	double valorRetidoCsll;
	double baseCalculoIrrf;
	double valorRetidoIrrf;
	double baseCalculoPrevidencia;
	double valorRetidoPrevidencia;
	String informacoesAddFisco;
	String informacoesAddContribuinte;
	String comexUfEmbarque;
	String comexLocalEmbarque;
	String comexLocalDespacho;
	String compraNotaEmpenho;
	String compraPedido;
	String compraContrato;
	String qrcode;
	String urlChave;
	String statusNota;
	NfeCana nfeCana;
	NfeDestinatario nfeDestinatario;
	NfeEmitente nfeEmitente;
	NfeFatura nfeFatura;
	NfeInformacaoPagamento nfeInformacaoPagamento;
	NfeLocalEntrega nfeLocalEntrega;
	NfeLocalRetirada nfeLocalRetirada;
	NfeResponsavelTecnico nfeResponsavelTecnico;
	NfeTransporte nfeTransporte;
	Vendedor vendedor;
	Fornecedor fornecedor;
	Cliente cliente;
	TributOperacaoFiscal tributOperacaoFiscal;
	VendaCabecalho vendaCabecalho;
	List<NfeAcessoXml> listaNfeAcessoXml = [];
	List<NfeCteReferenciado> listaNfeCteReferenciado = [];
	List<NfeCupomFiscalReferenciado> listaNfeCupomFiscalReferenciado = [];
	List<NfeDetalhe> listaNfeDetalhe = [];
	List<NfeNfReferenciada> listaNfeNfReferenciada = [];
	List<NfeProcessoReferenciado> listaNfeProcessoReferenciado = [];
	List<NfeProdRuralReferenciada> listaNfeProdRuralReferenciada = [];
	List<NfeReferenciada> listaNfeReferenciada = [];
	List<ViewSpedNfeDetalhe> listaViewSpedNfeDetalhe = [];

	NfeCabecalho({
		this.id,
		this.idVendedor,
		this.idFornecedor,
		this.idCliente,
		this.idTributOperacaoFiscal,
		this.idVendaCabecalho,
		this.idNfceMovimento,
		this.ufEmitente,
		this.codigoNumerico,
		this.naturezaOperacao,
		this.codigoModelo,
		this.serie,
		this.numero,
		this.dataHoraEmissao,
		this.dataHoraEntradaSaida,
		this.tipoOperacao = '1=Saída',
		this.localDestino = '1=Operação interna',
		this.codigoMunicipio,
		this.formatoImpressaoDanfe = '1=DANFE normal, Retrato',
		this.tipoEmissao = '1=Emissão normal',
		this.chaveAcesso,
		this.digitoChaveAcesso,
		this.ambiente = '1=Produção',
		this.finalidadeEmissao = '1=NF-e normal',
		this.consumidorOperacao = '0=Normal',
		this.consumidorPresenca = '1=Operação presencial',
		this.processoEmissao = '0=Emissão de NF-e com aplicativo do contribuinte',
		this.versaoProcessoEmissao,
		this.dataEntradaContingencia,
		this.justificativaContingencia,
		this.baseCalculoIcms = 0.0,
		this.valorIcms = 0.0,
		this.valorIcmsDesonerado = 0.0,
		this.totalIcmsFcpUfDestino = 0.0,
		this.totalIcmsInterestadualUfDestino = 0.0,
		this.totalIcmsInterestadualUfRemente = 0.0,
		this.valorTotalFcp = 0.0,
		this.baseCalculoIcmsSt = 0.0,
		this.valorIcmsSt = 0.0,
		this.valorTotalFcpSt = 0.0,
		this.valorTotalFcpStRetido = 0.0,
		this.valorTotalProdutos = 0.0,
		this.valorFrete = 0.0,
		this.valorSeguro = 0.0,
		this.valorDesconto = 0.0,
		this.valorImpostoImportacao = 0.0,
		this.valorIpi = 0.0,
		this.valorIpiDevolvido = 0.0,
		this.valorPis = 0.0,
		this.valorCofins = 0.0,
		this.valorDespesasAcessorias = 0.0,
		this.valorTotal = 0.0,
		this.valorTotalTributos = 0.0,
		this.valorServicos = 0.0,
		this.baseCalculoIssqn = 0.0,
		this.valorIssqn = 0.0,
		this.valorPisIssqn = 0.0,
		this.valorCofinsIssqn = 0.0,
		this.dataPrestacaoServico,
		this.valorDeducaoIssqn = 0.0,
		this.outrasRetencoesIssqn = 0.0,
		this.descontoIncondicionadoIssqn = 0.0,
		this.descontoCondicionadoIssqn = 0.0,
		this.totalRetencaoIssqn = 0.0,
		this.regimeEspecialTributacao,
		this.valorRetidoPis = 0.0,
		this.valorRetidoCofins = 0.0,
		this.valorRetidoCsll = 0.0,
		this.baseCalculoIrrf = 0.0,
		this.valorRetidoIrrf = 0.0,
		this.baseCalculoPrevidencia = 0.0,
		this.valorRetidoPrevidencia = 0.0,
		this.informacoesAddFisco,
		this.informacoesAddContribuinte,
		this.comexUfEmbarque,
		this.comexLocalEmbarque,
		this.comexLocalDespacho,
		this.compraNotaEmpenho,
		this.compraPedido,
		this.compraContrato,
		this.qrcode,
		this.urlChave,
		this.statusNota,
		this.nfeCana,
		this.nfeDestinatario,
		this.nfeEmitente,
		this.nfeFatura,
		this.nfeInformacaoPagamento,
		this.nfeLocalEntrega,
		this.nfeLocalRetirada,
		this.nfeResponsavelTecnico,
		this.nfeTransporte,
		this.vendedor,
		this.fornecedor,
		this.cliente,
		this.tributOperacaoFiscal,
		this.vendaCabecalho,
		this.listaNfeAcessoXml,
		this.listaNfeCteReferenciado,
		this.listaNfeCupomFiscalReferenciado,
		this.listaNfeDetalhe,
		this.listaNfeNfReferenciada,
		this.listaNfeProcessoReferenciado,
		this.listaNfeProdRuralReferenciada,
		this.listaNfeReferenciada,
		this.listaViewSpedNfeDetalhe,
	});

	static List<String> campos = <String>[
		'ID', 
		'UF_EMITENTE', 
		'CODIGO_NUMERICO', 
		'NATUREZA_OPERACAO', 
		'CODIGO_MODELO', 
		'SERIE', 
		'NUMERO', 
		'DATA_HORA_EMISSAO', 
		'DATA_HORA_ENTRADA_SAIDA', 
		'TIPO_OPERACAO', 
		'LOCAL_DESTINO', 
		'CODIGO_MUNICIPIO', 
		'FORMATO_IMPRESSAO_DANFE', 
		'TIPO_EMISSAO', 
		'CHAVE_ACESSO', 
		'DIGITO_CHAVE_ACESSO', 
		'AMBIENTE', 
		'FINALIDADE_EMISSAO', 
		'CONSUMIDOR_OPERACAO', 
		'CONSUMIDOR_PRESENCA', 
		'PROCESSO_EMISSAO', 
		'VERSAO_PROCESSO_EMISSAO', 
		'DATA_ENTRADA_CONTINGENCIA', 
		'JUSTIFICATIVA_CONTINGENCIA', 
		'BASE_CALCULO_ICMS', 
		'VALOR_ICMS', 
		'VALOR_ICMS_DESONERADO', 
		'TOTAL_ICMS_FCP_UF_DESTINO', 
		'TOTAL_ICMS_INTERESTADUAL_UF_DESTINO', 
		'TOTAL_ICMS_INTERESTADUAL_UF_REMENTE', 
		'VALOR_TOTAL_FCP', 
		'BASE_CALCULO_ICMS_ST', 
		'VALOR_ICMS_ST', 
		'VALOR_TOTAL_FCP_ST', 
		'VALOR_TOTAL_FCP_ST_RETIDO', 
		'VALOR_TOTAL_PRODUTOS', 
		'VALOR_FRETE', 
		'VALOR_SEGURO', 
		'VALOR_DESCONTO', 
		'VALOR_IMPOSTO_IMPORTACAO', 
		'VALOR_IPI', 
		'VALOR_IPI_DEVOLVIDO', 
		'VALOR_PIS', 
		'VALOR_COFINS', 
		'VALOR_DESPESAS_ACESSORIAS', 
		'VALOR_TOTAL', 
		'VALOR_TOTAL_TRIBUTOS', 
		'VALOR_SERVICOS', 
		'BASE_CALCULO_ISSQN', 
		'VALOR_ISSQN', 
		'VALOR_PIS_ISSQN', 
		'VALOR_COFINS_ISSQN', 
		'DATA_PRESTACAO_SERVICO', 
		'VALOR_DEDUCAO_ISSQN', 
		'OUTRAS_RETENCOES_ISSQN', 
		'DESCONTO_INCONDICIONADO_ISSQN', 
		'DESCONTO_CONDICIONADO_ISSQN', 
		'TOTAL_RETENCAO_ISSQN', 
		'REGIME_ESPECIAL_TRIBUTACAO', 
		'VALOR_RETIDO_PIS', 
		'VALOR_RETIDO_COFINS', 
		'VALOR_RETIDO_CSLL', 
		'BASE_CALCULO_IRRF', 
		'VALOR_RETIDO_IRRF', 
		'BASE_CALCULO_PREVIDENCIA', 
		'VALOR_RETIDO_PREVIDENCIA', 
		'INFORMACOES_ADD_FISCO', 
		'INFORMACOES_ADD_CONTRIBUINTE', 
		'COMEX_UF_EMBARQUE', 
		'COMEX_LOCAL_EMBARQUE', 
		'COMEX_LOCAL_DESPACHO', 
		'COMPRA_NOTA_EMPENHO', 
		'COMPRA_PEDIDO', 
		'COMPRA_CONTRATO', 
		'QRCODE', 
		'URL_CHAVE', 
		'STATUS_NOTA', 
	];
	
	static List<String> colunas = <String>[
		'Id', 
		'Código UF', 
		'Código Numérico', 
		'Natureza da Operação', 
		'Código Modelo', 
		'Série', 
		'Número', 
		'Data/Hora Emissão', 
		'Data/Hora Entrada/Saída', 
		'Tipo Operação', 
		'Local Destino', 
		'Município IBGE', 
		'Formato de Impressão do DANFE', 
		'Tipo de Emissão', 
		'Chave de Acesso', 
		'Dígito Chave de Acesso', 
		'Ambiente', 
		'Finalidade Emissão', 
		'Consumidor Final', 
		'Indicador de Presença do Consumidor', 
		'Processo de Emissão', 
		'Versão Processo Emissão', 
		'Data Contingência', 
		'Justificativa Contingência', 
		'Base Cálculo ICMS', 
		'Valor do ICMS', 
		'Valor do ICMS Desonerado', 
		'Total ICMS FCP UF Destino', 
		'Total ICMS Interestadual UF Destino', 
		'Total ICMS Interestadual UF Remetente', 
		'Total FCP', 
		'Base Cálculo ICMS ST', 
		'Valor do ICMS ST', 
		'Valor Total FCP ST', 
		'Valor Total FCP ST Retido', 
		'Valor Total Produtos', 
		'Valor Frete', 
		'Valor Seguro', 
		'Valor Desconto', 
		'Valor Imposto Importação', 
		'Valor IPI', 
		'Valor IPI Devolvido', 
		'Valor PIS', 
		'Valor COFINS', 
		'Valor Despesas Acessórias', 
		'Valor Total NF', 
		'Valor Total Tributos', 
		'Valor Total Serviços', 
		'Base Cálculo ISSQN', 
		'Valor ISSQN', 
		'Valor PIS ISSQN', 
		'Valor COFINS ISSQN', 
		'Data Prestação do Serviço', 
		'Valor Dedução ISSQN', 
		'Outras Retenções ISSQN', 
		'Valor Desconto Incondicionado ISSQN', 
		'Valor Desconto Condicionado ISSQN', 
		'Total Retenção ISSQN', 
		'Código do Regime Especial de Tributação', 
		'Valor Retido PIS', 
		'Valor Retido COFINS', 
		'Valor Retido CSLL', 
		'Valor Base Cálculo IRRF', 
		'Valor Retido IRRF', 
		'Valor Base Cálculo Retenção Previdência', 
		'Valor Retido Previdência', 
		'Informações Adicionais Fisco', 
		'Informações Adicionais Contribuinte', 
		'UF de Embarque', 
		'Local de Embarque', 
		'Local de Despacho', 
		'Nota de Empenho', 
		'Pedido Compra', 
		'Contrato Compra', 
		'QRCode', 
		'URL Chave', 
		'Status Nota', 
	];

	NfeCabecalho.fromJson(Map<String, dynamic> jsonDados) {
		id = jsonDados['id'];
		idVendedor = jsonDados['idVendedor'];
		idFornecedor = jsonDados['idFornecedor'];
		idCliente = jsonDados['idCliente'];
		idTributOperacaoFiscal = jsonDados['idTributOperacaoFiscal'];
		idVendaCabecalho = jsonDados['idVendaCabecalho'];
		idNfceMovimento = jsonDados['idNfceMovimento'];
		ufEmitente = jsonDados['ufEmitente'];
		codigoNumerico = jsonDados['codigoNumerico'];
		naturezaOperacao = jsonDados['naturezaOperacao'];
		codigoModelo = jsonDados['codigoModelo'];
		serie = jsonDados['serie'];
		numero = jsonDados['numero'];
		dataHoraEmissao = jsonDados['dataHoraEmissao'] != null ? DateTime.tryParse(jsonDados['dataHoraEmissao']) : null;
		dataHoraEntradaSaida = jsonDados['dataHoraEntradaSaida'] != null ? DateTime.tryParse(jsonDados['dataHoraEntradaSaida']) : null;
		tipoOperacao = getTipoOperacao(jsonDados['tipoOperacao']);
		localDestino = getLocalDestino(jsonDados['localDestino']);
		codigoMunicipio = jsonDados['codigoMunicipio'];
		formatoImpressaoDanfe = getFormatoImpressaoDanfe(jsonDados['formatoImpressaoDanfe']);
		tipoEmissao = getTipoEmissao(jsonDados['tipoEmissao']);
		chaveAcesso = jsonDados['chaveAcesso'];
		digitoChaveAcesso = jsonDados['digitoChaveAcesso'];
		ambiente = getAmbiente(jsonDados['ambiente']);
		finalidadeEmissao = getFinalidadeEmissao(jsonDados['finalidadeEmissao']);
		consumidorOperacao = getConsumidorOperacao(jsonDados['consumidorOperacao']);
		consumidorPresenca = getConsumidorPresenca(jsonDados['consumidorPresenca']);
		processoEmissao = getProcessoEmissao(jsonDados['processoEmissao']);
		versaoProcessoEmissao = jsonDados['versaoProcessoEmissao'];
		dataEntradaContingencia = jsonDados['dataEntradaContingencia'] != null ? DateTime.tryParse(jsonDados['dataEntradaContingencia']) : null;
		justificativaContingencia = jsonDados['justificativaContingencia'];
		baseCalculoIcms = jsonDados['baseCalculoIcms'] != null ? jsonDados['baseCalculoIcms'].toDouble() : null;
		valorIcms = jsonDados['valorIcms'] != null ? jsonDados['valorIcms'].toDouble() : null;
		valorIcmsDesonerado = jsonDados['valorIcmsDesonerado'] != null ? jsonDados['valorIcmsDesonerado'].toDouble() : null;
		totalIcmsFcpUfDestino = jsonDados['totalIcmsFcpUfDestino'] != null ? jsonDados['totalIcmsFcpUfDestino'].toDouble() : null;
		totalIcmsInterestadualUfDestino = jsonDados['totalIcmsInterestadualUfDestino'] != null ? jsonDados['totalIcmsInterestadualUfDestino'].toDouble() : null;
		totalIcmsInterestadualUfRemente = jsonDados['totalIcmsInterestadualUfRemente'] != null ? jsonDados['totalIcmsInterestadualUfRemente'].toDouble() : null;
		valorTotalFcp = jsonDados['valorTotalFcp'] != null ? jsonDados['valorTotalFcp'].toDouble() : null;
		baseCalculoIcmsSt = jsonDados['baseCalculoIcmsSt'] != null ? jsonDados['baseCalculoIcmsSt'].toDouble() : null;
		valorIcmsSt = jsonDados['valorIcmsSt'] != null ? jsonDados['valorIcmsSt'].toDouble() : null;
		valorTotalFcpSt = jsonDados['valorTotalFcpSt'] != null ? jsonDados['valorTotalFcpSt'].toDouble() : null;
		valorTotalFcpStRetido = jsonDados['valorTotalFcpStRetido'] != null ? jsonDados['valorTotalFcpStRetido'].toDouble() : null;
		valorTotalProdutos = jsonDados['valorTotalProdutos'] != null ? jsonDados['valorTotalProdutos'].toDouble() : null;
		valorFrete = jsonDados['valorFrete'] != null ? jsonDados['valorFrete'].toDouble() : null;
		valorSeguro = jsonDados['valorSeguro'] != null ? jsonDados['valorSeguro'].toDouble() : null;
		valorDesconto = jsonDados['valorDesconto'] != null ? jsonDados['valorDesconto'].toDouble() : null;
		valorImpostoImportacao = jsonDados['valorImpostoImportacao'] != null ? jsonDados['valorImpostoImportacao'].toDouble() : null;
		valorIpi = jsonDados['valorIpi'] != null ? jsonDados['valorIpi'].toDouble() : null;
		valorIpiDevolvido = jsonDados['valorIpiDevolvido'] != null ? jsonDados['valorIpiDevolvido'].toDouble() : null;
		valorPis = jsonDados['valorPis'] != null ? jsonDados['valorPis'].toDouble() : null;
		valorCofins = jsonDados['valorCofins'] != null ? jsonDados['valorCofins'].toDouble() : null;
		valorDespesasAcessorias = jsonDados['valorDespesasAcessorias'] != null ? jsonDados['valorDespesasAcessorias'].toDouble() : null;
		valorTotal = jsonDados['valorTotal'] != null ? jsonDados['valorTotal'].toDouble() : null;
		valorTotalTributos = jsonDados['valorTotalTributos'] != null ? jsonDados['valorTotalTributos'].toDouble() : null;
		valorServicos = jsonDados['valorServicos'] != null ? jsonDados['valorServicos'].toDouble() : null;
		baseCalculoIssqn = jsonDados['baseCalculoIssqn'] != null ? jsonDados['baseCalculoIssqn'].toDouble() : null;
		valorIssqn = jsonDados['valorIssqn'] != null ? jsonDados['valorIssqn'].toDouble() : null;
		valorPisIssqn = jsonDados['valorPisIssqn'] != null ? jsonDados['valorPisIssqn'].toDouble() : null;
		valorCofinsIssqn = jsonDados['valorCofinsIssqn'] != null ? jsonDados['valorCofinsIssqn'].toDouble() : null;
		dataPrestacaoServico = jsonDados['dataPrestacaoServico'] != null ? DateTime.tryParse(jsonDados['dataPrestacaoServico']) : null;
		valorDeducaoIssqn = jsonDados['valorDeducaoIssqn'] != null ? jsonDados['valorDeducaoIssqn'].toDouble() : null;
		outrasRetencoesIssqn = jsonDados['outrasRetencoesIssqn'] != null ? jsonDados['outrasRetencoesIssqn'].toDouble() : null;
		descontoIncondicionadoIssqn = jsonDados['descontoIncondicionadoIssqn'] != null ? jsonDados['descontoIncondicionadoIssqn'].toDouble() : null;
		descontoCondicionadoIssqn = jsonDados['descontoCondicionadoIssqn'] != null ? jsonDados['descontoCondicionadoIssqn'].toDouble() : null;
		totalRetencaoIssqn = jsonDados['totalRetencaoIssqn'] != null ? jsonDados['totalRetencaoIssqn'].toDouble() : null;
		regimeEspecialTributacao = getRegimeEspecialTributacao(jsonDados['regimeEspecialTributacao']);
		valorRetidoPis = jsonDados['valorRetidoPis'] != null ? jsonDados['valorRetidoPis'].toDouble() : null;
		valorRetidoCofins = jsonDados['valorRetidoCofins'] != null ? jsonDados['valorRetidoCofins'].toDouble() : null;
		valorRetidoCsll = jsonDados['valorRetidoCsll'] != null ? jsonDados['valorRetidoCsll'].toDouble() : null;
		baseCalculoIrrf = jsonDados['baseCalculoIrrf'] != null ? jsonDados['baseCalculoIrrf'].toDouble() : null;
		valorRetidoIrrf = jsonDados['valorRetidoIrrf'] != null ? jsonDados['valorRetidoIrrf'].toDouble() : null;
		baseCalculoPrevidencia = jsonDados['baseCalculoPrevidencia'] != null ? jsonDados['baseCalculoPrevidencia'].toDouble() : null;
		valorRetidoPrevidencia = jsonDados['valorRetidoPrevidencia'] != null ? jsonDados['valorRetidoPrevidencia'].toDouble() : null;
		informacoesAddFisco = jsonDados['informacoesAddFisco'];
		informacoesAddContribuinte = jsonDados['informacoesAddContribuinte'];
		comexUfEmbarque = jsonDados['comexUfEmbarque'] == '' ? null : jsonDados['comexUfEmbarque'];
		comexLocalEmbarque = jsonDados['comexLocalEmbarque'];
		comexLocalDespacho = jsonDados['comexLocalDespacho'];
		compraNotaEmpenho = jsonDados['compraNotaEmpenho'];
		compraPedido = jsonDados['compraPedido'];
		compraContrato = jsonDados['compraContrato'];
		qrcode = jsonDados['qrcode'];
		urlChave = jsonDados['urlChave'];
		statusNota = getStatusNota(jsonDados['statusNota']);
		nfeCana = jsonDados['nfeCana'] == null ? null : NfeCana.fromJson(jsonDados['nfeCana']);
		nfeDestinatario = jsonDados['nfeDestinatario'] == null ? null : NfeDestinatario.fromJson(jsonDados['nfeDestinatario']);
		nfeEmitente = jsonDados['nfeEmitente'] == null ? null : NfeEmitente.fromJson(jsonDados['nfeEmitente']);
		nfeFatura = jsonDados['nfeFatura'] == null ? null : NfeFatura.fromJson(jsonDados['nfeFatura']);
		nfeInformacaoPagamento = jsonDados['nfeInformacaoPagamento'] == null ? null : NfeInformacaoPagamento.fromJson(jsonDados['nfeInformacaoPagamento']);
		nfeLocalEntrega = jsonDados['nfeLocalEntrega'] == null ? null : NfeLocalEntrega.fromJson(jsonDados['nfeLocalEntrega']);
		nfeLocalRetirada = jsonDados['nfeLocalRetirada'] == null ? null : NfeLocalRetirada.fromJson(jsonDados['nfeLocalRetirada']);
		nfeResponsavelTecnico = jsonDados['nfeResponsavelTecnico'] == null ? null : NfeResponsavelTecnico.fromJson(jsonDados['nfeResponsavelTecnico']);
		nfeTransporte = jsonDados['nfeTransporte'] == null ? null : NfeTransporte.fromJson(jsonDados['nfeTransporte']);
		vendedor = jsonDados['vendedor'] == null ? null : Vendedor.fromJson(jsonDados['vendedor']);
		fornecedor = jsonDados['fornecedor'] == null ? null : Fornecedor.fromJson(jsonDados['fornecedor']);
		cliente = jsonDados['cliente'] == null ? null : Cliente.fromJson(jsonDados['cliente']);
		tributOperacaoFiscal = jsonDados['tributOperacaoFiscal'] == null ? null : TributOperacaoFiscal.fromJson(jsonDados['tributOperacaoFiscal']);
		vendaCabecalho = jsonDados['vendaCabecalho'] == null ? null : VendaCabecalho.fromJson(jsonDados['vendaCabecalho']);
		listaNfeAcessoXml = (jsonDados['listaNfeAcessoXml'] as Iterable)?.map((m) => NfeAcessoXml.fromJson(m))?.toList() ?? [];
		listaNfeCteReferenciado = (jsonDados['listaNfeCteReferenciado'] as Iterable)?.map((m) => NfeCteReferenciado.fromJson(m))?.toList() ?? [];
		listaNfeCupomFiscalReferenciado = (jsonDados['listaNfeCupomFiscalReferenciado'] as Iterable)?.map((m) => NfeCupomFiscalReferenciado.fromJson(m))?.toList() ?? [];
		listaNfeDetalhe = (jsonDados['listaNfeDetalhe'] as Iterable)?.map((m) => NfeDetalhe.fromJson(m))?.toList() ?? [];
		listaNfeNfReferenciada = (jsonDados['listaNfeNfReferenciada'] as Iterable)?.map((m) => NfeNfReferenciada.fromJson(m))?.toList() ?? [];
		listaNfeProcessoReferenciado = (jsonDados['listaNfeProcessoReferenciado'] as Iterable)?.map((m) => NfeProcessoReferenciado.fromJson(m))?.toList() ?? [];
		listaNfeProdRuralReferenciada = (jsonDados['listaNfeProdRuralReferenciada'] as Iterable)?.map((m) => NfeProdRuralReferenciada.fromJson(m))?.toList() ?? [];
		listaNfeReferenciada = (jsonDados['listaNfeReferenciada'] as Iterable)?.map((m) => NfeReferenciada.fromJson(m))?.toList() ?? [];
		listaViewSpedNfeDetalhe = (jsonDados['listaViewSpedNfeDetalhe'] as Iterable)?.map((m) => ViewSpedNfeDetalhe.fromJson(m))?.toList() ?? [];
	}

	Map<String, dynamic> get toJson {
		Map<String, dynamic> jsonDados = Map<String, dynamic>();

		jsonDados['id'] = this.id ?? 0;
		jsonDados['idVendedor'] = this.idVendedor ?? 0;
		jsonDados['idFornecedor'] = this.idFornecedor ?? 0;
		jsonDados['idCliente'] = this.idCliente ?? 0;
		jsonDados['idTributOperacaoFiscal'] = this.idTributOperacaoFiscal ?? 0;
		jsonDados['idVendaCabecalho'] = this.idVendaCabecalho ?? 0;
		jsonDados['idNfceMovimento'] = this.idNfceMovimento ?? 0;
		jsonDados['ufEmitente'] = this.ufEmitente ?? 0;
		jsonDados['codigoNumerico'] = this.codigoNumerico;
		jsonDados['naturezaOperacao'] = this.naturezaOperacao;
		jsonDados['codigoModelo'] = this.codigoModelo;
		jsonDados['serie'] = this.serie;
		jsonDados['numero'] = this.numero;
		jsonDados['dataHoraEmissao'] = this.dataHoraEmissao != null ? DateFormat('yyyy-MM-ddT00:00:00').format(this.dataHoraEmissao) : null;
		jsonDados['dataHoraEntradaSaida'] = this.dataHoraEntradaSaida != null ? DateFormat('yyyy-MM-ddT00:00:00').format(this.dataHoraEntradaSaida) : null;
		jsonDados['tipoOperacao'] = setTipoOperacao(this.tipoOperacao);
		jsonDados['localDestino'] = setLocalDestino(this.localDestino);
		jsonDados['codigoMunicipio'] = this.codigoMunicipio ?? 0;
		jsonDados['formatoImpressaoDanfe'] = setFormatoImpressaoDanfe(this.formatoImpressaoDanfe);
		jsonDados['tipoEmissao'] = setTipoEmissao(this.tipoEmissao);
		jsonDados['chaveAcesso'] = this.chaveAcesso;
		jsonDados['digitoChaveAcesso'] = this.digitoChaveAcesso;
		jsonDados['ambiente'] = setAmbiente(this.ambiente);
		jsonDados['finalidadeEmissao'] = setFinalidadeEmissao(this.finalidadeEmissao);
		jsonDados['consumidorOperacao'] = setConsumidorOperacao(this.consumidorOperacao);
		jsonDados['consumidorPresenca'] = setConsumidorPresenca(this.consumidorPresenca);
		jsonDados['processoEmissao'] = setProcessoEmissao(this.processoEmissao);
		jsonDados['versaoProcessoEmissao'] = this.versaoProcessoEmissao;
		jsonDados['dataEntradaContingencia'] = this.dataEntradaContingencia != null ? DateFormat('yyyy-MM-ddT00:00:00').format(this.dataEntradaContingencia) : null;
		jsonDados['justificativaContingencia'] = this.justificativaContingencia;
		jsonDados['baseCalculoIcms'] = this.baseCalculoIcms;
		jsonDados['valorIcms'] = this.valorIcms;
		jsonDados['valorIcmsDesonerado'] = this.valorIcmsDesonerado;
		jsonDados['totalIcmsFcpUfDestino'] = this.totalIcmsFcpUfDestino;
		jsonDados['totalIcmsInterestadualUfDestino'] = this.totalIcmsInterestadualUfDestino;
		jsonDados['totalIcmsInterestadualUfRemente'] = this.totalIcmsInterestadualUfRemente;
		jsonDados['valorTotalFcp'] = this.valorTotalFcp;
		jsonDados['baseCalculoIcmsSt'] = this.baseCalculoIcmsSt;
		jsonDados['valorIcmsSt'] = this.valorIcmsSt;
		jsonDados['valorTotalFcpSt'] = this.valorTotalFcpSt;
		jsonDados['valorTotalFcpStRetido'] = this.valorTotalFcpStRetido;
		jsonDados['valorTotalProdutos'] = this.valorTotalProdutos;
		jsonDados['valorFrete'] = this.valorFrete;
		jsonDados['valorSeguro'] = this.valorSeguro;
		jsonDados['valorDesconto'] = this.valorDesconto;
		jsonDados['valorImpostoImportacao'] = this.valorImpostoImportacao;
		jsonDados['valorIpi'] = this.valorIpi;
		jsonDados['valorIpiDevolvido'] = this.valorIpiDevolvido;
		jsonDados['valorPis'] = this.valorPis;
		jsonDados['valorCofins'] = this.valorCofins;
		jsonDados['valorDespesasAcessorias'] = this.valorDespesasAcessorias;
		jsonDados['valorTotal'] = this.valorTotal;
		jsonDados['valorTotalTributos'] = this.valorTotalTributos;
		jsonDados['valorServicos'] = this.valorServicos;
		jsonDados['baseCalculoIssqn'] = this.baseCalculoIssqn;
		jsonDados['valorIssqn'] = this.valorIssqn;
		jsonDados['valorPisIssqn'] = this.valorPisIssqn;
		jsonDados['valorCofinsIssqn'] = this.valorCofinsIssqn;
		jsonDados['dataPrestacaoServico'] = this.dataPrestacaoServico != null ? DateFormat('yyyy-MM-ddT00:00:00').format(this.dataPrestacaoServico) : null;
		jsonDados['valorDeducaoIssqn'] = this.valorDeducaoIssqn;
		jsonDados['outrasRetencoesIssqn'] = this.outrasRetencoesIssqn;
		jsonDados['descontoIncondicionadoIssqn'] = this.descontoIncondicionadoIssqn;
		jsonDados['descontoCondicionadoIssqn'] = this.descontoCondicionadoIssqn;
		jsonDados['totalRetencaoIssqn'] = this.totalRetencaoIssqn;
		jsonDados['regimeEspecialTributacao'] = setRegimeEspecialTributacao(this.regimeEspecialTributacao);
		jsonDados['valorRetidoPis'] = this.valorRetidoPis;
		jsonDados['valorRetidoCofins'] = this.valorRetidoCofins;
		jsonDados['valorRetidoCsll'] = this.valorRetidoCsll;
		jsonDados['baseCalculoIrrf'] = this.baseCalculoIrrf;
		jsonDados['valorRetidoIrrf'] = this.valorRetidoIrrf;
		jsonDados['baseCalculoPrevidencia'] = this.baseCalculoPrevidencia;
		jsonDados['valorRetidoPrevidencia'] = this.valorRetidoPrevidencia;
		jsonDados['informacoesAddFisco'] = this.informacoesAddFisco;
		jsonDados['informacoesAddContribuinte'] = this.informacoesAddContribuinte;
		jsonDados['comexUfEmbarque'] = this.comexUfEmbarque;
		jsonDados['comexLocalEmbarque'] = this.comexLocalEmbarque;
		jsonDados['comexLocalDespacho'] = this.comexLocalDespacho;
		jsonDados['compraNotaEmpenho'] = this.compraNotaEmpenho;
		jsonDados['compraPedido'] = this.compraPedido;
		jsonDados['compraContrato'] = this.compraContrato;
		jsonDados['qrcode'] = this.qrcode;
		jsonDados['urlChave'] = this.urlChave;
		jsonDados['statusNota'] = setStatusNota(this.statusNota);
		jsonDados['nfeCana'] = this.nfeCana == null ? null : this.nfeCana.toJson;
		jsonDados['nfeDestinatario'] = this.nfeDestinatario == null ? null : this.nfeDestinatario.toJson;
		jsonDados['nfeEmitente'] = this.nfeEmitente == null ? null : this.nfeEmitente.toJson;
		jsonDados['nfeFatura'] = this.nfeFatura == null ? null : this.nfeFatura.toJson;
		jsonDados['nfeInformacaoPagamento'] = this.nfeInformacaoPagamento == null ? null : this.nfeInformacaoPagamento.toJson;
		jsonDados['nfeLocalEntrega'] = this.nfeLocalEntrega == null ? null : this.nfeLocalEntrega.toJson;
		jsonDados['nfeLocalRetirada'] = this.nfeLocalRetirada == null ? null : this.nfeLocalRetirada.toJson;
		jsonDados['nfeResponsavelTecnico'] = this.nfeResponsavelTecnico == null ? null : this.nfeResponsavelTecnico.toJson;
		jsonDados['nfeTransporte'] = this.nfeTransporte == null ? null : this.nfeTransporte.toJson;
		jsonDados['vendedor'] = this.vendedor == null ? null : this.vendedor.toJson;
		jsonDados['fornecedor'] = this.fornecedor == null ? null : this.fornecedor.toJson;
		jsonDados['cliente'] = this.cliente == null ? null : this.cliente.toJson;
		jsonDados['tributOperacaoFiscal'] = this.tributOperacaoFiscal == null ? null : this.tributOperacaoFiscal.toJson;
		jsonDados['vendaCabecalho'] = this.vendaCabecalho == null ? null : this.vendaCabecalho.toJson;
		

		var listaNfeAcessoXmlLocal = [];
		for (NfeAcessoXml objeto in this.listaNfeAcessoXml ?? []) {
			listaNfeAcessoXmlLocal.add(objeto.toJson);
		}
		jsonDados['listaNfeAcessoXml'] = listaNfeAcessoXmlLocal;
		

		var listaNfeCteReferenciadoLocal = [];
		for (NfeCteReferenciado objeto in this.listaNfeCteReferenciado ?? []) {
			listaNfeCteReferenciadoLocal.add(objeto.toJson);
		}
		jsonDados['listaNfeCteReferenciado'] = listaNfeCteReferenciadoLocal;
		

		var listaNfeCupomFiscalReferenciadoLocal = [];
		for (NfeCupomFiscalReferenciado objeto in this.listaNfeCupomFiscalReferenciado ?? []) {
			listaNfeCupomFiscalReferenciadoLocal.add(objeto.toJson);
		}
		jsonDados['listaNfeCupomFiscalReferenciado'] = listaNfeCupomFiscalReferenciadoLocal;
		

		var listaNfeDetalheLocal = [];
		for (NfeDetalhe objeto in this.listaNfeDetalhe ?? []) {
			listaNfeDetalheLocal.add(objeto.toJson);
		}
		jsonDados['listaNfeDetalhe'] = listaNfeDetalheLocal;
		

		var listaNfeNfReferenciadaLocal = [];
		for (NfeNfReferenciada objeto in this.listaNfeNfReferenciada ?? []) {
			listaNfeNfReferenciadaLocal.add(objeto.toJson);
		}
		jsonDados['listaNfeNfReferenciada'] = listaNfeNfReferenciadaLocal;
		

		var listaNfeProcessoReferenciadoLocal = [];
		for (NfeProcessoReferenciado objeto in this.listaNfeProcessoReferenciado ?? []) {
			listaNfeProcessoReferenciadoLocal.add(objeto.toJson);
		}
		jsonDados['listaNfeProcessoReferenciado'] = listaNfeProcessoReferenciadoLocal;
		

		var listaNfeProdRuralReferenciadaLocal = [];
		for (NfeProdRuralReferenciada objeto in this.listaNfeProdRuralReferenciada ?? []) {
			listaNfeProdRuralReferenciadaLocal.add(objeto.toJson);
		}
		jsonDados['listaNfeProdRuralReferenciada'] = listaNfeProdRuralReferenciadaLocal;
		

		var listaNfeReferenciadaLocal = [];
		for (NfeReferenciada objeto in this.listaNfeReferenciada ?? []) {
			listaNfeReferenciadaLocal.add(objeto.toJson);
		}
		jsonDados['listaNfeReferenciada'] = listaNfeReferenciadaLocal;
		

		var listaViewSpedNfeDetalheLocal = [];
		for (ViewSpedNfeDetalhe objeto in this.listaViewSpedNfeDetalhe ?? []) {
			listaViewSpedNfeDetalheLocal.add(objeto.toJson);
		}
		jsonDados['listaViewSpedNfeDetalhe'] = listaViewSpedNfeDetalheLocal;
	
		return jsonDados;
	}
	
    getTipoOperacao(String tipoOperacao) {
    	switch (tipoOperacao) {
    		case '0':
    			return '0=Entrada';
    			break;
    		case '1':
    			return '1=Saída';
    			break;
    		default:
    			return null;
    		}
    	}

    setTipoOperacao(String tipoOperacao) {
    	switch (tipoOperacao) {
    		case '0=Entrada':
    			return '0';
    			break;
    		case '1=Saída':
    			return '1';
    			break;
    		default:
    			return null;
    		}
    	}

    getLocalDestino(String localDestino) {
    	switch (localDestino) {
    		case '1':
    			return '1=Operação interna';
    			break;
    		case '2':
    			return '2=Operação interestadual';
    			break;
    		case '3':
    			return '3=Operação com exterior';
    			break;
    		default:
    			return null;
    		}
    	}

    setLocalDestino(String localDestino) {
    	switch (localDestino) {
    		case '1=Operação interna':
    			return '1';
    			break;
    		case '2=Operação interestadual':
    			return '2';
    			break;
    		case '3=Operação com exterior':
    			return '3';
    			break;
    		default:
    			return null;
    		}
    	}

    getFormatoImpressaoDanfe(String formatoImpressaoDanfe) {
    	switch (formatoImpressaoDanfe) {
    		case '0':
    			return '0=Sem geração de DANFE';
    			break;
    		case '1':
    			return '1=DANFE normal, Retrato';
    			break;
    		case '2':
    			return '2=DANFE normal, Paisagem';
    			break;
    		case '3':
    			return '3=DANFE Simplificado';
    			break;
    		default:
    			return null;
    		}
    	}

    setFormatoImpressaoDanfe(String formatoImpressaoDanfe) {
    	switch (formatoImpressaoDanfe) {
    		case '0=Sem geração de DANFE':
    			return '0';
    			break;
    		case '1=DANFE normal, Retrato':
    			return '1';
    			break;
    		case '2=DANFE normal, Paisagem':
    			return '2';
    			break;
    		case '3=DANFE Simplificado':
    			return '3';
    			break;
    		default:
    			return null;
    		}
    	}

    getTipoEmissao(String tipoEmissao) {
    	switch (tipoEmissao) {
    		case '1':
    			return '1=Emissão normal';
    			break;
    		case '2':
    			return '2=Contingência FS-IA';
    			break;
    		case '4':
    			return '4=Contingência EPEC';
    			break;
    		case '5':
    			return '5=Contingência FS-DA';
    			break;
    		case '6':
    			return '6=Contingência SVC-AN';
    			break;
    		case '7':
    			return '7=Contingência SVC-RS';
    			break;
    		default:
    			return null;
    		}
    	}

    setTipoEmissao(String tipoEmissao) {
    	switch (tipoEmissao) {
    		case '1=Emissão normal':
    			return '1';
    			break;
    		case '2=Contingência FS-IA':
    			return '2';
    			break;
    		case '4=Contingência EPEC':
    			return '4';
    			break;
    		case '5=Contingência FS-DA':
    			return '5';
    			break;
    		case '6=Contingência SVC-AN':
    			return '6';
    			break;
    		case '7=Contingência SVC-RS':
    			return '7';
    			break;
    		default:
    			return null;
    		}
    	}

    getAmbiente(String ambiente) {
    	switch (ambiente) {
    		case '1':
    			return '1=Produção';
    			break;
    		case '2':
    			return '2=Homologação';
    			break;
    		default:
    			return null;
    		}
    	}

    setAmbiente(String ambiente) {
    	switch (ambiente) {
    		case '1=Produção':
    			return '1';
    			break;
    		case '2=Homologação':
    			return '2';
    			break;
    		default:
    			return null;
    		}
    	}

    getFinalidadeEmissao(String finalidadeEmissao) {
    	switch (finalidadeEmissao) {
    		case '1':
    			return '1=NF-e normal';
    			break;
    		case '2':
    			return '2=NF-e complementar';
    			break;
    		case '3':
    			return '3=NF-e de ajuste';
    			break;
    		case '4':
    			return '4=Devolução de mercadoria';
    			break;
    		default:
    			return null;
    		}
    	}

    setFinalidadeEmissao(String finalidadeEmissao) {
    	switch (finalidadeEmissao) {
    		case '1=NF-e normal':
    			return '1';
    			break;
    		case '2=NF-e complementar':
    			return '2';
    			break;
    		case '3=NF-e de ajuste':
    			return '3';
    			break;
    		case '4=Devolução de mercadoria':
    			return '4';
    			break;
    		default:
    			return null;
    		}
    	}

    getConsumidorOperacao(String consumidorOperacao) {
    	switch (consumidorOperacao) {
    		case '0':
    			return '0=Normal';
    			break;
    		case '1':
    			return '1=Consumidor final';
    			break;
    		default:
    			return null;
    		}
    	}

    setConsumidorOperacao(String consumidorOperacao) {
    	switch (consumidorOperacao) {
    		case '0=Normal':
    			return '0';
    			break;
    		case '1=Consumidor final':
    			return '1';
    			break;
    		default:
    			return null;
    		}
    	}

    getConsumidorPresenca(String consumidorPresenca) {
    	switch (consumidorPresenca) {
    		case '0':
    			return '0=Não se aplica';
    			break;
    		case '1':
    			return '1=Operação presencial';
    			break;
    		case '2':
    			return '2=Operação não presencial, pela Internet';
    			break;
    		case '3':
    			return '3=Operação não presencial, Teleatendimento';
    			break;
    		case '4':
    			return '4=NFC-e em operação com entrega a domicílio';
    			break;
    		case '5':
    			return '5=Operação presencial, fora do estabelecimento';
    			break;
    		case '9':
    			return '9=Operação não presencial, outros';
    			break;
    		default:
    			return null;
    		}
    	}

    setConsumidorPresenca(String consumidorPresenca) {
    	switch (consumidorPresenca) {
    		case '0=Não se aplica':
    			return '0';
    			break;
    		case '1=Operação presencial':
    			return '1';
    			break;
    		case '2=Operação não presencial, pela Internet':
    			return '2';
    			break;
    		case '3=Operação não presencial, Teleatendimento':
    			return '3';
    			break;
    		case '4=NFC-e em operação com entrega a domicílio':
    			return '4';
    			break;
    		case '5=Operação presencial, fora do estabelecimento':
    			return '5';
    			break;
    		case '9=Operação não presencial, outros':
    			return '9';
    			break;
    		default:
    			return null;
    		}
    	}

    getProcessoEmissao(String processoEmissao) {
    	switch (processoEmissao) {
    		case '0':
    			return '0=Emissão de NF-e com aplicativo do contribuinte';
    			break;
    		case '1':
    			return '1=Emissão de NF-e avulsa pelo Fisco';
    			break;
    		case '2':
    			return '2=Emissão de NF-e avulsa, pelo contribuinte com seu certificado digital, através do site do Fisco';
    			break;
    		case '3':
    			return '3=Emissão NF-e pelo contribuinte com aplicativo fornecido pelo Fisco';
    			break;
    		default:
    			return null;
    		}
    	}

    setProcessoEmissao(String processoEmissao) {
    	switch (processoEmissao) {
    		case '0=Emissão de NF-e com aplicativo do contribuinte':
    			return '0';
    			break;
    		case '1=Emissão de NF-e avulsa pelo Fisco':
    			return '1';
    			break;
    		case '2=Emissão de NF-e avulsa, pelo contribuinte com seu certificado digital, através do site do Fisco':
    			return '2';
    			break;
    		case '3=Emissão NF-e pelo contribuinte com aplicativo fornecido pelo Fisco':
    			return '3';
    			break;
    		default:
    			return null;
    		}
    	}

    getRegimeEspecialTributacao(String regimeEspecialTributacao) {
    	switch (regimeEspecialTributacao) {
    		case '0':
    			return '0=Emissão de NF-e com aplicativo do contribuinte';
    			break;
    		case '1':
    			return '1=Microempresa Municipal';
    			break;
    		case '2':
    			return '2=Estimativa';
    			break;
    		case '3':
    			return '3=Sociedade de Profissionais';
    			break;
    		case '4':
    			return '4=Cooperativa';
    			break;
    		case '5':
    			return '5=Microempresário Individual (MEI)';
    			break;
    		case '6':
    			return '6=Microempresário e Empresa de Pequeno Porte';
    			break;
    		default:
    			return null;
    		}
    	}

    setRegimeEspecialTributacao(String regimeEspecialTributacao) {
    	switch (regimeEspecialTributacao) {
    		case '0=Emissão de NF-e com aplicativo do contribuinte':
    			return '0';
    			break;
    		case '1=Microempresa Municipal':
    			return '1';
    			break;
    		case '2=Estimativa':
    			return '2';
    			break;
    		case '3=Sociedade de Profissionais':
    			return '3';
    			break;
    		case '4=Cooperativa':
    			return '4';
    			break;
    		case '5=Microempresário Individual (MEI)':
    			return '5';
    			break;
    		case '6=Microempresário e Empresa de Pequeno Porte':
    			return '6';
    			break;
    		default:
    			return null;
    		}
    	}

    getStatusNota(String statusNota) {
    	switch (statusNota) {
    		case '0':
    			return '0-Em Edição';
    			break;
    		case '1':
    			return '1-Salva';
    			break;
    		case '2':
    			return '2-Validada';
    			break;
    		case '3':
    			return '3-Assinada';
    			break;
    		case '4':
    			return '4-Autorizada';
    			break;
    		case '5':
    			return '5-Inutilizada';
    			break;
    		case '6':
    			return '6-Cancelada';
    			break;
    		default:
    			return null;
    		}
    	}

    setStatusNota(String statusNota) {
    	switch (statusNota) {
    		case '0-Em Edição':
    			return '0';
    			break;
    		case '1-Salva':
    			return '1';
    			break;
    		case '2-Validada':
    			return '2';
    			break;
    		case '3-Assinada':
    			return '3';
    			break;
    		case '4-Autorizada':
    			return '4';
    			break;
    		case '5-Inutilizada':
    			return '5';
    			break;
    		case '6-Cancelada':
    			return '6';
    			break;
    		default:
    			return null;
    		}
    	}


	String objetoEncodeJson(NfeCabecalho objeto) {
	  final jsonDados = objeto.toJson;
	  return json.encode(jsonDados);
	}
	
}