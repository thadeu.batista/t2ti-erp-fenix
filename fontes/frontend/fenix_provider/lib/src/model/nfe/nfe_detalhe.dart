/*
Title: T2Ti ERP 3.0                                                                
Description: Model relacionado à tabela [NFE_DETALHE] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2021 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
import 'dart:convert';

import 'package:fenix/src/infra/biblioteca.dart';
import 'package:fenix/src/model/model.dart';

class NfeDetalhe {
	int id;
	int idNfeCabecalho;
	int idProduto;
	int numeroItem;
	String codigoProduto;
	String gtin;
	String nomeProduto;
	String ncm;
	String nve;
	String cest;
	String indicadorEscalaRelevante;
	String cnpjFabricante;
	String codigoBeneficioFiscal;
	int exTipi;
	int cfop;
	String unidadeComercial;
	double quantidadeComercial;
	String numeroPedidoCompra;
	int itemPedidoCompra;
	String numeroFci;
	String numeroRecopi;
	double valorUnitarioComercial;
	double valorBrutoProduto;
	String gtinUnidadeTributavel;
	String unidadeTributavel;
	double quantidadeTributavel;
	double valorUnitarioTributavel;
	double valorFrete;
	double valorSeguro;
	double valorDesconto;
	double valorOutrasDespesas;
	String entraTotal;
	double valorTotalTributos;
	double percentualDevolvido;
	double valorIpiDevolvido;
	String informacoesAdicionais;
	double valorSubtotal;
	double valorTotal;
	Produto produto;

	NfeDetalhe({
		this.id,
		this.idNfeCabecalho,
		this.idProduto,
		this.numeroItem,
		this.codigoProduto,
		this.gtin,
		this.nomeProduto,
		this.ncm,
		this.nve,
		this.cest,
		this.indicadorEscalaRelevante,
		this.cnpjFabricante,
		this.codigoBeneficioFiscal,
		this.exTipi,
		this.cfop,
		this.unidadeComercial,
		this.quantidadeComercial = 0.0,
		this.numeroPedidoCompra,
		this.itemPedidoCompra,
		this.numeroFci,
		this.numeroRecopi,
		this.valorUnitarioComercial = 0.0,
		this.valorBrutoProduto = 0.0,
		this.gtinUnidadeTributavel,
		this.unidadeTributavel,
		this.quantidadeTributavel = 0.0,
		this.valorUnitarioTributavel = 0.0,
		this.valorFrete = 0.0,
		this.valorSeguro = 0.0,
		this.valorDesconto = 0.0,
		this.valorOutrasDespesas = 0.0,
		this.entraTotal,
		this.valorTotalTributos = 0.0,
		this.percentualDevolvido = 0.0,
		this.valorIpiDevolvido = 0.0,
		this.informacoesAdicionais,
		this.valorSubtotal = 0.0,
		this.valorTotal = 0.0,
		this.produto,
	});

	static List<String> campos = <String>[
		'ID', 
		'NUMERO_ITEM', 
		'CODIGO_PRODUTO', 
		'GTIN', 
		'NOME_PRODUTO', 
		'NCM', 
		'NVE', 
		'CEST', 
		'INDICADOR_ESCALA_RELEVANTE', 
		'CNPJ_FABRICANTE', 
		'CODIGO_BENEFICIO_FISCAL', 
		'EX_TIPI', 
		'CFOP', 
		'UNIDADE_COMERCIAL', 
		'QUANTIDADE_COMERCIAL', 
		'NUMERO_PEDIDO_COMPRA', 
		'ITEM_PEDIDO_COMPRA', 
		'NUMERO_FCI', 
		'NUMERO_RECOPI', 
		'VALOR_UNITARIO_COMERCIAL', 
		'VALOR_BRUTO_PRODUTO', 
		'GTIN_UNIDADE_TRIBUTAVEL', 
		'UNIDADE_TRIBUTAVEL', 
		'QUANTIDADE_TRIBUTAVEL', 
		'VALOR_UNITARIO_TRIBUTAVEL', 
		'VALOR_FRETE', 
		'VALOR_SEGURO', 
		'VALOR_DESCONTO', 
		'VALOR_OUTRAS_DESPESAS', 
		'ENTRA_TOTAL', 
		'VALOR_TOTAL_TRIBUTOS', 
		'PERCENTUAL_DEVOLVIDO', 
		'VALOR_IPI_DEVOLVIDO', 
		'INFORMACOES_ADICIONAIS', 
		'VALOR_SUBTOTAL', 
		'VALOR_TOTAL', 
	];
	
	static List<String> colunas = <String>[
		'Id', 
		'Número do Item', 
		'Código do Produto', 
		'GTIN', 
		'Nome', 
		'NCM', 
		'NVE', 
		'CEST', 
		'Indicador Escala Relevante', 
		'CNPJ do Fabricante', 
		'Código Benefício Fiscal', 
		'EX TIPI', 
		'CFOP', 
		'Unidade Comercial', 
		'Quantidade Comercial', 
		'Número Pedido Compra', 
		'Item Pedido Compra', 
		'Número FCI', 
		'Número RECOPI', 
		'Valor Unitário Comercial', 
		'Valor Bruto Produto', 
		'GTIN Unidade Tributável', 
		'Unidade Tributável', 
		'Quantidade Tributável', 
		'Valor Unitário Tributável', 
		'Valor Frete', 
		'Valor Seguro', 
		'Valor Desconto', 
		'Valor Outras Despesas', 
		'Indicador Exibilidade', 
		'Valor Total Tributos', 
		'Percentual Devolvido', 
		'Valor IPI Devolvido', 
		'Informações Adicionais', 
		'Valor Subtotal', 
		'Valor Total', 
	];

	NfeDetalhe.fromJson(Map<String, dynamic> jsonDados) {
		id = jsonDados['id'];
		idNfeCabecalho = jsonDados['idNfeCabecalho'];
		idProduto = jsonDados['idProduto'];
		numeroItem = jsonDados['numeroItem'];
		codigoProduto = jsonDados['codigoProduto'];
		gtin = jsonDados['gtin'];
		nomeProduto = jsonDados['nomeProduto'];
		ncm = jsonDados['ncm'];
		nve = jsonDados['nve'];
		cest = jsonDados['cest'];
		indicadorEscalaRelevante = getIndicadorEscalaRelevante(jsonDados['indicadorEscalaRelevante']);
		cnpjFabricante = jsonDados['cnpjFabricante'];
		codigoBeneficioFiscal = jsonDados['codigoBeneficioFiscal'];
		exTipi = jsonDados['exTipi'];
		cfop = jsonDados['cfop'];
		unidadeComercial = jsonDados['unidadeComercial'];
		quantidadeComercial = jsonDados['quantidadeComercial'] != null ? jsonDados['quantidadeComercial'].toDouble() : null;
		numeroPedidoCompra = jsonDados['numeroPedidoCompra'];
		itemPedidoCompra = jsonDados['itemPedidoCompra'];
		numeroFci = jsonDados['numeroFci'];
		numeroRecopi = jsonDados['numeroRecopi'];
		valorUnitarioComercial = jsonDados['valorUnitarioComercial'] != null ? jsonDados['valorUnitarioComercial'].toDouble() : null;
		valorBrutoProduto = jsonDados['valorBrutoProduto'] != null ? jsonDados['valorBrutoProduto'].toDouble() : null;
		gtinUnidadeTributavel = jsonDados['gtinUnidadeTributavel'];
		unidadeTributavel = jsonDados['unidadeTributavel'];
		quantidadeTributavel = jsonDados['quantidadeTributavel'] != null ? jsonDados['quantidadeTributavel'].toDouble() : null;
		valorUnitarioTributavel = jsonDados['valorUnitarioTributavel'] != null ? jsonDados['valorUnitarioTributavel'].toDouble() : null;
		valorFrete = jsonDados['valorFrete'] != null ? jsonDados['valorFrete'].toDouble() : null;
		valorSeguro = jsonDados['valorSeguro'] != null ? jsonDados['valorSeguro'].toDouble() : null;
		valorDesconto = jsonDados['valorDesconto'] != null ? jsonDados['valorDesconto'].toDouble() : null;
		valorOutrasDespesas = jsonDados['valorOutrasDespesas'] != null ? jsonDados['valorOutrasDespesas'].toDouble() : null;
		entraTotal = getEntraTotal(jsonDados['entraTotal']);
		valorTotalTributos = jsonDados['valorTotalTributos'] != null ? jsonDados['valorTotalTributos'].toDouble() : null;
		percentualDevolvido = jsonDados['percentualDevolvido'] != null ? jsonDados['percentualDevolvido'].toDouble() : null;
		valorIpiDevolvido = jsonDados['valorIpiDevolvido'] != null ? jsonDados['valorIpiDevolvido'].toDouble() : null;
		informacoesAdicionais = jsonDados['informacoesAdicionais'];
		valorSubtotal = jsonDados['valorSubtotal'] != null ? jsonDados['valorSubtotal'].toDouble() : null;
		valorTotal = jsonDados['valorTotal'] != null ? jsonDados['valorTotal'].toDouble() : null;
		produto = jsonDados['produto'] == null ? null : Produto.fromJson(jsonDados['produto']);
	}

	Map<String, dynamic> get toJson {
		Map<String, dynamic> jsonDados = Map<String, dynamic>();

		jsonDados['id'] = this.id ?? 0;
		jsonDados['idNfeCabecalho'] = this.idNfeCabecalho ?? 0;
		jsonDados['idProduto'] = this.idProduto ?? 0;
		jsonDados['numeroItem'] = this.numeroItem ?? 0;
		jsonDados['codigoProduto'] = this.codigoProduto;
		jsonDados['gtin'] = this.gtin;
		jsonDados['nomeProduto'] = this.nomeProduto;
		jsonDados['ncm'] = this.ncm;
		jsonDados['nve'] = this.nve;
		jsonDados['cest'] = this.cest;
		jsonDados['indicadorEscalaRelevante'] = setIndicadorEscalaRelevante(this.indicadorEscalaRelevante);
		jsonDados['cnpjFabricante'] = Biblioteca.removerMascara(this.cnpjFabricante);
		jsonDados['codigoBeneficioFiscal'] = this.codigoBeneficioFiscal;
		jsonDados['exTipi'] = this.exTipi ?? 0;
		jsonDados['cfop'] = this.cfop ?? 0;
		jsonDados['unidadeComercial'] = this.unidadeComercial;
		jsonDados['quantidadeComercial'] = this.quantidadeComercial;
		jsonDados['numeroPedidoCompra'] = this.numeroPedidoCompra;
		jsonDados['itemPedidoCompra'] = this.itemPedidoCompra ?? 0;
		jsonDados['numeroFci'] = this.numeroFci;
		jsonDados['numeroRecopi'] = this.numeroRecopi;
		jsonDados['valorUnitarioComercial'] = this.valorUnitarioComercial;
		jsonDados['valorBrutoProduto'] = this.valorBrutoProduto;
		jsonDados['gtinUnidadeTributavel'] = this.gtinUnidadeTributavel;
		jsonDados['unidadeTributavel'] = this.unidadeTributavel;
		jsonDados['quantidadeTributavel'] = this.quantidadeTributavel;
		jsonDados['valorUnitarioTributavel'] = this.valorUnitarioTributavel;
		jsonDados['valorFrete'] = this.valorFrete;
		jsonDados['valorSeguro'] = this.valorSeguro;
		jsonDados['valorDesconto'] = this.valorDesconto;
		jsonDados['valorOutrasDespesas'] = this.valorOutrasDespesas;
		jsonDados['entraTotal'] = setEntraTotal(this.entraTotal);
		jsonDados['valorTotalTributos'] = this.valorTotalTributos;
		jsonDados['percentualDevolvido'] = this.percentualDevolvido;
		jsonDados['valorIpiDevolvido'] = this.valorIpiDevolvido;
		jsonDados['informacoesAdicionais'] = this.informacoesAdicionais;
		jsonDados['valorSubtotal'] = this.valorSubtotal;
		jsonDados['valorTotal'] = this.valorTotal;
		jsonDados['produto'] = this.produto == null ? null : this.produto.toJson;
	
		return jsonDados;
	}
	
    getIndicadorEscalaRelevante(String indicadorEscalaRelevante) {
    	switch (indicadorEscalaRelevante) {
    		case 'S':
    			return 'Sim';
    			break;
    		case 'N':
    			return 'Não';
    			break;
    		default:
    			return null;
    		}
    	}

    setIndicadorEscalaRelevante(String indicadorEscalaRelevante) {
    	switch (indicadorEscalaRelevante) {
    		case 'Sim':
    			return 'S';
    			break;
    		case 'Não':
    			return 'N';
    			break;
    		default:
    			return null;
    		}
    	}

    getEntraTotal(String entraTotal) {
    	switch (entraTotal) {
    		case '0':
    			return '0=Valor do item (vProd) não compõe o valor total da NF-e';
    			break;
    		case '1':
    			return '1=Valor do item (vProd) compõe o valor total da NF-e (vProd)';
    			break;
    		default:
    			return null;
    		}
    	}

    setEntraTotal(String entraTotal) {
    	switch (entraTotal) {
    		case '0=Valor do item (vProd) não compõe o valor total da NF-e':
    			return '0';
    			break;
    		case '1=Valor do item (vProd) compõe o valor total da NF-e (vProd)':
    			return '1';
    			break;
    		default:
    			return null;
    		}
    	}


	String objetoEncodeJson(NfeDetalhe objeto) {
	  final jsonDados = objeto.toJson;
	  return json.encode(jsonDados);
	}
	
}