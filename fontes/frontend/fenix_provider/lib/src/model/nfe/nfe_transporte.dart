/*
Title: T2Ti ERP 3.0                                                                
Description: Model relacionado à tabela [NFE_TRANSPORTE] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2021 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
import 'dart:convert';

import 'package:fenix/src/infra/biblioteca.dart';
import 'package:fenix/src/model/model.dart';

class NfeTransporte {
	int id;
	int idNfeCabecalho;
	int idTransportadora;
	String modalidadeFrete;
	String cnpj;
	String cpf;
	String nome;
	String inscricaoEstadual;
	String endereco;
	String nomeMunicipio;
	String uf;
	double valorServico;
	double valorBcRetencaoIcms;
	double aliquotaRetencaoIcms;
	double valorIcmsRetido;
	int cfop;
	int municipio;
	String placaVeiculo;
	String ufVeiculo;
	String rntcVeiculo;
	Transportadora transportadora;

	NfeTransporte({
		this.id,
		this.idNfeCabecalho,
		this.idTransportadora,
		this.modalidadeFrete,
		this.cnpj,
		this.cpf,
		this.nome,
		this.inscricaoEstadual,
		this.endereco,
		this.nomeMunicipio,
		this.uf,
		this.valorServico = 0.0,
		this.valorBcRetencaoIcms = 0.0,
		this.aliquotaRetencaoIcms = 0.0,
		this.valorIcmsRetido = 0.0,
		this.cfop,
		this.municipio,
		this.placaVeiculo,
		this.ufVeiculo,
		this.rntcVeiculo,
		this.transportadora,
	});

	static List<String> campos = <String>[
		'ID', 
		'MODALIDADE_FRETE', 
		'CNPJ', 
		'CPF', 
		'NOME', 
		'INSCRICAO_ESTADUAL', 
		'ENDERECO', 
		'NOME_MUNICIPIO', 
		'UF', 
		'VALOR_SERVICO', 
		'VALOR_BC_RETENCAO_ICMS', 
		'ALIQUOTA_RETENCAO_ICMS', 
		'VALOR_ICMS_RETIDO', 
		'CFOP', 
		'MUNICIPIO', 
		'PLACA_VEICULO', 
		'UF_VEICULO', 
		'RNTC_VEICULO', 
	];
	
	static List<String> colunas = <String>[
		'Id', 
		'Modalidade Frete', 
		'CNPJ', 
		'CPF', 
		'Nome', 
		'Incrição Estadual', 
		'Endereço', 
		'Nome Município', 
		'UF', 
		'Valor Serviço', 
		'Valor BC Retenção ICMS', 
		'Alíquota Retenção ICMS', 
		'Valor ICMS Retido', 
		'CFOP', 
		'Município IBGE', 
		'Placa Veículo', 
		'UF', 
		'RNTC Veículo', 
	];

	NfeTransporte.fromJson(Map<String, dynamic> jsonDados) {
		id = jsonDados['id'];
		idNfeCabecalho = jsonDados['idNfeCabecalho'];
		idTransportadora = jsonDados['idTransportadora'];
		modalidadeFrete = getModalidadeFrete(jsonDados['modalidadeFrete']);
		cnpj = jsonDados['cnpj'];
		cpf = jsonDados['cpf'];
		nome = jsonDados['nome'];
		inscricaoEstadual = jsonDados['inscricaoEstadual'];
		endereco = jsonDados['endereco'];
		nomeMunicipio = jsonDados['nomeMunicipio'];
		uf = jsonDados['uf'] == '' ? null : jsonDados['uf'];
		valorServico = jsonDados['valorServico'] != null ? jsonDados['valorServico'].toDouble() : null;
		valorBcRetencaoIcms = jsonDados['valorBcRetencaoIcms'] != null ? jsonDados['valorBcRetencaoIcms'].toDouble() : null;
		aliquotaRetencaoIcms = jsonDados['aliquotaRetencaoIcms'] != null ? jsonDados['aliquotaRetencaoIcms'].toDouble() : null;
		valorIcmsRetido = jsonDados['valorIcmsRetido'] != null ? jsonDados['valorIcmsRetido'].toDouble() : null;
		cfop = jsonDados['cfop'];
		municipio = jsonDados['municipio'];
		placaVeiculo = jsonDados['placaVeiculo'];
		ufVeiculo = jsonDados['ufVeiculo'] == '' ? null : jsonDados['ufVeiculo'];
		rntcVeiculo = jsonDados['rntcVeiculo'];
		transportadora = jsonDados['transportadora'] == null ? null : Transportadora.fromJson(jsonDados['transportadora']);
	}

	Map<String, dynamic> get toJson {
		Map<String, dynamic> jsonDados = Map<String, dynamic>();

		jsonDados['id'] = this.id ?? 0;
		jsonDados['idNfeCabecalho'] = this.idNfeCabecalho ?? 0;
		jsonDados['idTransportadora'] = this.idTransportadora ?? 0;
		jsonDados['modalidadeFrete'] = setModalidadeFrete(this.modalidadeFrete);
		jsonDados['cnpj'] = Biblioteca.removerMascara(this.cnpj);
		jsonDados['cpf'] = Biblioteca.removerMascara(this.cpf);
		jsonDados['nome'] = this.nome;
		jsonDados['inscricaoEstadual'] = this.inscricaoEstadual;
		jsonDados['endereco'] = this.endereco;
		jsonDados['nomeMunicipio'] = this.nomeMunicipio;
		jsonDados['uf'] = this.uf;
		jsonDados['valorServico'] = this.valorServico;
		jsonDados['valorBcRetencaoIcms'] = this.valorBcRetencaoIcms;
		jsonDados['aliquotaRetencaoIcms'] = this.aliquotaRetencaoIcms;
		jsonDados['valorIcmsRetido'] = this.valorIcmsRetido;
		jsonDados['cfop'] = this.cfop ?? 0;
		jsonDados['municipio'] = this.municipio ?? 0;
		jsonDados['placaVeiculo'] = this.placaVeiculo;
		jsonDados['ufVeiculo'] = this.ufVeiculo;
		jsonDados['rntcVeiculo'] = this.rntcVeiculo;
		jsonDados['transportadora'] = this.transportadora == null ? null : this.transportadora.toJson;
	
		return jsonDados;
	}
	
    getModalidadeFrete(String modalidadeFrete) {
    	switch (modalidadeFrete) {
    		case '0':
    			return '0=Contratação do Frete por conta do Remetente (CIF)';
    			break;
    		case '1':
    			return '1=Contratação do Frete por conta do Destinatário (FOB)';
    			break;
    		case '2':
    			return '2=Contratação do Frete por conta de Terceiros';
    			break;
    		case '3':
    			return '3=Transporte Próprio por conta do Remetente';
    			break;
    		case '4':
    			return '4=Transporte Próprio por conta do Destinatário';
    			break;
    		case '9':
    			return '9=Sem Ocorrência de Transporte';
    			break;
    		default:
    			return null;
    		}
    	}

    setModalidadeFrete(String modalidadeFrete) {
    	switch (modalidadeFrete) {
    		case '0=Contratação do Frete por conta do Remetente (CIF)':
    			return '0';
    			break;
    		case '1=Contratação do Frete por conta do Destinatário (FOB)':
    			return '1';
    			break;
    		case '2=Contratação do Frete por conta de Terceiros':
    			return '2';
    			break;
    		case '3=Transporte Próprio por conta do Remetente':
    			return '3';
    			break;
    		case '4=Transporte Próprio por conta do Destinatário':
    			return '4';
    			break;
    		case '9=Sem Ocorrência de Transporte':
    			return '9';
    			break;
    		default:
    			return null;
    		}
    	}


	String objetoEncodeJson(NfeTransporte objeto) {
	  final jsonDados = objeto.toJson;
	  return json.encode(jsonDados);
	}
	
}