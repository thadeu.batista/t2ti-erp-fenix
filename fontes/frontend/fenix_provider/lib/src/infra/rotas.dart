/*
Title: T2Ti ERP 3.0                                                                
Description: Define as rotas da aplicação
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2021 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
import 'package:flutter/material.dart';

import 'package:fenix/src/view/menu/home_page.dart';
// import 'package:fenix/src/view/login/login_page.dart';
import 'package:fenix/src/view/menu/menu_cadastros.dart';
import 'package:fenix/src/view/menu/menu_financeiro.dart';
import 'package:fenix/src/view/menu/menu_tributacao.dart';
import 'package:fenix/src/view/menu/menu_estoque.dart';
import 'package:fenix/src/view/menu/menu_vendas.dart';
import 'package:fenix/src/view/menu/menu_compras.dart';
import 'package:fenix/src/view/menu/menu_comissoes.dart';
import 'package:fenix/src/view/menu/menu_os.dart';
import 'package:fenix/src/view/menu/menu_afv.dart';
import 'package:fenix/src/view/menu/menu_nfse.dart';
import 'package:fenix/src/view/menu/menu_nfe.dart';

// import 'package:fenix/src/model/model.dart';
import 'package:fenix/src/view/page/page.dart';

class Rotas {
  static Route<dynamic> definirRotas(RouteSettings settings) {
    switch (settings.name) {
      
      // Login
      case '/':
        return MaterialPageRoute(builder: (_) => HomePage());//LoginPage());

      // Home
      case '/home':
        return MaterialPageRoute(builder: (_) => HomePage());

      // Menus
      case '/cadastrosMenu':
        return MaterialPageRoute(builder: (_) => MenuCadastros());
      case '/financeiroMenu':
        return MaterialPageRoute(builder: (_) => MenuFinanceiro());
      case '/tributacaoMenu':
        return MaterialPageRoute(builder: (_) => MenuTributacao());
      case '/estoqueMenu':
        return MaterialPageRoute(builder: (_) => MenuEstoque());
      case '/vendasMenu':
        return MaterialPageRoute(builder: (_) => MenuVendas());
      case '/comprasMenu':
        return MaterialPageRoute(builder: (_) => MenuCompras());
      case '/comissoesMenu':
        return MaterialPageRoute(builder: (_) => MenuComissoes());
      case '/ordemServicoMenu':
        return MaterialPageRoute(builder: (_) => MenuOrdemServico());
      case '/afvMenu':
        return MaterialPageRoute(builder: (_) => MenuAFV());
      case '/nfseMenu':
        return MaterialPageRoute(builder: (_) => MenuNFSe());
      case '/nfeMenu':
        return MaterialPageRoute(builder: (_) => MenuNFe());
		
      ////////////////////////////////////////////////////////// 
      /// CADASTROS
      //////////////////////////////////////////////////////////

      // Banco
      case '/bancoLista':
        return MaterialPageRoute(builder: (_) => BancoListaPage());
      case '/bancoPersiste':
        return MaterialPageRoute(builder: (_) => BancoPersistePage());

      // BancoAgencia
      case '/bancoAgenciaLista':
        return MaterialPageRoute(builder: (_) => BancoAgenciaListaPage());
      case '/bancoAgenciaPersiste':
        return MaterialPageRoute(builder: (_) => BancoAgenciaPersistePage());

      // Pessoa
      case '/pessoaLista':
        return MaterialPageRoute(builder: (_) => PessoaListaPage());
      case '/pessoaPersiste':
        return MaterialPageRoute(builder: (_) => PessoaPersistePage());

      // Produto
      case '/produtoLista':
        return MaterialPageRoute(builder: (_) => ProdutoListaPage());
      case '/produtoPersiste':
        return MaterialPageRoute(builder: (_) => ProdutoPersistePage());

			// BancoContaCaixa
			case '/bancoContaCaixaLista':
			  return MaterialPageRoute(builder: (_) => BancoContaCaixaListaPage());
			case '/bancoContaCaixaPersiste':
			  return MaterialPageRoute(builder: (_) => BancoContaCaixaPersistePage());

			// Cargo
			case '/cargoLista':
			  return MaterialPageRoute(builder: (_) => CargoListaPage());
			case '/cargoPersiste':
			  return MaterialPageRoute(builder: (_) => CargoPersistePage());

			// Cep
			case '/cepLista':
			  return MaterialPageRoute(builder: (_) => CepListaPage());
			case '/cepPersiste':
			  return MaterialPageRoute(builder: (_) => CepPersistePage());

			// Cfop
			case '/cfopLista':
			  return MaterialPageRoute(builder: (_) => CfopListaPage());
			case '/cfopPersiste':
			  return MaterialPageRoute(builder: (_) => CfopPersistePage());
			
			// Cnae
			case '/cnaeLista':
			  return MaterialPageRoute(builder: (_) => CnaeListaPage());
			case '/cnaePersiste':
			  return MaterialPageRoute(builder: (_) => CnaePersistePage());

			// Setor
			case '/setorLista':
			  return MaterialPageRoute(builder: (_) => SetorListaPage());
			case '/setorPersiste':
			  return MaterialPageRoute(builder: (_) => SetorPersistePage());
			  			
			// Csosn
			case '/csosnLista':
			  return MaterialPageRoute(builder: (_) => CsosnListaPage());
			case '/csosnPersiste':
			  return MaterialPageRoute(builder: (_) => CsosnPersistePage());
			
			// CstCofins
			case '/cstCofinsLista':
			  return MaterialPageRoute(builder: (_) => CstCofinsListaPage());
			case '/cstCofinsPersiste':
			  return MaterialPageRoute(builder: (_) => CstCofinsPersistePage());
			
			// CstIcms
			case '/cstIcmsLista':
			  return MaterialPageRoute(builder: (_) => CstIcmsListaPage());
			case '/cstIcmsPersiste':
			  return MaterialPageRoute(builder: (_) => CstIcmsPersistePage());
			
			// CstIpi
			case '/cstIpiLista':
			  return MaterialPageRoute(builder: (_) => CstIpiListaPage());
			case '/cstIpiPersiste':
			  return MaterialPageRoute(builder: (_) => CstIpiPersistePage());
			
			// CstPis
			case '/cstPisLista':
			  return MaterialPageRoute(builder: (_) => CstPisListaPage());
			case '/cstPisPersiste':
			  return MaterialPageRoute(builder: (_) => CstPisPersistePage());

			// EstadoCivil
			case '/estadoCivilLista':
			  return MaterialPageRoute(builder: (_) => EstadoCivilListaPage());
			case '/estadoCivilPersiste':
			  return MaterialPageRoute(builder: (_) => EstadoCivilPersistePage());
						
			// Municipio
			case '/municipioLista':
			  return MaterialPageRoute(builder: (_) => MunicipioListaPage());
			case '/municipioPersiste':
			  return MaterialPageRoute(builder: (_) => MunicipioPersistePage());
			
			// Ncm
			case '/ncmLista':
			  return MaterialPageRoute(builder: (_) => NcmListaPage());
			case '/ncmPersiste':
			  return MaterialPageRoute(builder: (_) => NcmPersistePage());
			
			// NivelFormacao
			case '/nivelFormacaoLista':
			  return MaterialPageRoute(builder: (_) => NivelFormacaoListaPage());
			case '/nivelFormacaoPersiste':
			  return MaterialPageRoute(builder: (_) => NivelFormacaoPersistePage());
									
			// Uf
			case '/ufLista':
			  return MaterialPageRoute(builder: (_) => UfListaPage());
			case '/ufPersiste':
			  return MaterialPageRoute(builder: (_) => UfPersistePage());
						
			// ProdutoGrupo
			case '/produtoGrupoLista':
			  return MaterialPageRoute(builder: (_) => ProdutoGrupoListaPage());
			case '/produtoGrupoPersiste':
			  return MaterialPageRoute(builder: (_) => ProdutoGrupoPersistePage());
			
			// ProdutoMarca
			case '/produtoMarcaLista':
			  return MaterialPageRoute(builder: (_) => ProdutoMarcaListaPage());
			case '/produtoMarcaPersiste':
			  return MaterialPageRoute(builder: (_) => ProdutoMarcaPersistePage());
			
			// ProdutoSubgrupo
			case '/produtoSubgrupoLista':
			  return MaterialPageRoute(builder: (_) => ProdutoSubgrupoListaPage());
			case '/produtoSubgrupoPersiste':
			  return MaterialPageRoute(builder: (_) => ProdutoSubgrupoPersistePage());
			
			// ProdutoUnidade
			case '/produtoUnidadeLista':
			  return MaterialPageRoute(builder: (_) => ProdutoUnidadeListaPage());
			case '/produtoUnidadePersiste':
			  return MaterialPageRoute(builder: (_) => ProdutoUnidadePersistePage());


      ////////////////////////////////////////////////////////// 
      /// BLOCO FINANCEIRO
      //////////////////////////////////////////////////////////

			// FinChequeEmitido
			case '/finChequeEmitidoLista':
			  return MaterialPageRoute(builder: (_) => FinChequeEmitidoListaPage());
			case '/finChequeEmitidoPersiste':
			  return MaterialPageRoute(builder: (_) => FinChequeEmitidoPersistePage());
			
			// FinChequeRecebido
			case '/finChequeRecebidoLista':
			  return MaterialPageRoute(builder: (_) => FinChequeRecebidoListaPage());
			case '/finChequeRecebidoPersiste':
			  return MaterialPageRoute(builder: (_) => FinChequeRecebidoPersistePage());
			
			// FinConfiguracaoBoleto
			case '/finConfiguracaoBoletoLista':
			  return MaterialPageRoute(builder: (_) => FinConfiguracaoBoletoListaPage());
			case '/finConfiguracaoBoletoPersiste':
			  return MaterialPageRoute(builder: (_) => FinConfiguracaoBoletoPersistePage());
			
			// FinDocumentoOrigem
			case '/finDocumentoOrigemLista':
			  return MaterialPageRoute(builder: (_) => FinDocumentoOrigemListaPage());
			case '/finDocumentoOrigemPersiste':
			  return MaterialPageRoute(builder: (_) => FinDocumentoOrigemPersistePage());
			
			// FinExtratoContaBanco
			case '/finExtratoContaBancoLista':
			  return MaterialPageRoute(builder: (_) => FinExtratoContaBancoListaPage());
			case '/finExtratoContaBancoPersiste':
			  return MaterialPageRoute(builder: (_) => FinExtratoContaBancoPersistePage());
			
			// FinFechamentoCaixaBanco
			case '/finFechamentoCaixaBancoLista':
			  return MaterialPageRoute(builder: (_) => FinFechamentoCaixaBancoListaPage());
			case '/finFechamentoCaixaBancoPersiste':
			  return MaterialPageRoute(builder: (_) => FinFechamentoCaixaBancoPersistePage());
			
			// FinLancamentoPagar
			case '/finLancamentoPagarLista':
			  return MaterialPageRoute(builder: (_) => FinLancamentoPagarListaPage());
			case '/finLancamentoPagarPersiste':
			  return MaterialPageRoute(builder: (_) => FinLancamentoPagarPersistePage());
			
			// FinLancamentoReceber
			case '/finLancamentoReceberLista':
			  return MaterialPageRoute(builder: (_) => FinLancamentoReceberListaPage());
			case '/finLancamentoReceberPersiste':
			  return MaterialPageRoute(builder: (_) => FinLancamentoReceberPersistePage());
			
			// FinNaturezaFinanceira
			case '/finNaturezaFinanceiraLista':
			  return MaterialPageRoute(builder: (_) => FinNaturezaFinanceiraListaPage());
			case '/finNaturezaFinanceiraPersiste':
			  return MaterialPageRoute(builder: (_) => FinNaturezaFinanceiraPersistePage());

			// FinStatusParcela
			case '/finStatusParcelaLista':
			  return MaterialPageRoute(builder: (_) => FinStatusParcelaListaPage());
			case '/finStatusParcelaPersiste':
			  return MaterialPageRoute(builder: (_) => FinStatusParcelaPersistePage());
			
			// FinTipoPagamento
			case '/finTipoPagamentoLista':
			  return MaterialPageRoute(builder: (_) => FinTipoPagamentoListaPage());
			case '/finTipoPagamentoPersiste':
			  return MaterialPageRoute(builder: (_) => FinTipoPagamentoPersistePage());
			
			// FinTipoRecebimento
			case '/finTipoRecebimentoLista':
			  return MaterialPageRoute(builder: (_) => FinTipoRecebimentoListaPage());
			case '/finTipoRecebimentoPersiste':
			  return MaterialPageRoute(builder: (_) => FinTipoRecebimentoPersistePage());

			// TalonarioCheque
			case '/talonarioChequeLista':
			  return MaterialPageRoute(builder: (_) => TalonarioChequeListaPage());
			case '/talonarioChequePersiste':
			  return MaterialPageRoute(builder: (_) => TalonarioChequePersistePage());
			  
			// FinParcelaPagamento
			case '/finParcelaPagamentoLista':
			  return MaterialPageRoute(builder: (_) => ViewFinLancamentoPagarListaPage());
			case '/finParcelaPagarPersiste':
			  return MaterialPageRoute(builder: (_) => FinParcelaPagarPersistePage());

			// FinParcelaRecebimento
			case '/finParcelaRecebimentoLista':
			  return MaterialPageRoute(builder: (_) => ViewFinLancamentoReceberListaPage());
			case '/finParcelaReceberPersiste':
			  return MaterialPageRoute(builder: (_) => FinParcelaReceberPersistePage());
			  
			// ViewFinMovimentoCaixaBanco
			case '/viewFinMovimentoCaixaBancoLista':
			  return MaterialPageRoute(builder: (_) => ViewFinMovimentoCaixaBancoListaPage());

			// Resumo Tesouraria
			case '/resumoTesouraria':
			  return MaterialPageRoute(builder: (_) => ResumoTesourariaGeralPage());

			// Fluxo de Caixa
			case '/fluxoCaixa':
			  return MaterialPageRoute(builder: (_) => FluxoCaixaGeralPage());

			// Conciliação Bancária
			case '/conciliacaoBancaria':
			  return MaterialPageRoute(builder: (_) => ConciliacaoBancariaGeralPage());
			

      ////////////////////////////////////////////////////////// 
      /// TRIBUTAÇÃO
      //////////////////////////////////////////////////////////

			// TributConfiguraOfGt
			case '/tributConfiguraOfGtLista':
			  return MaterialPageRoute(builder: (_) => TributConfiguraOfGtListaPage());
			case '/tributConfiguraOfGtPersiste':
			  return MaterialPageRoute(builder: (_) => TributConfiguraOfGtPersistePage());
			
			// TributGrupoTributario
			case '/tributGrupoTributarioLista':
			  return MaterialPageRoute(builder: (_) => TributGrupoTributarioListaPage());
			case '/tributGrupoTributarioPersiste':
			  return MaterialPageRoute(builder: (_) => TributGrupoTributarioPersistePage());
			
			// TributIcmsCustomCab
			case '/tributIcmsCustomCabLista':
			  return MaterialPageRoute(builder: (_) => TributIcmsCustomCabListaPage());
			case '/tributIcmsCustomCabPersiste':
			  return MaterialPageRoute(builder: (_) => TributIcmsCustomCabPersistePage());
												
			// TributIss
			case '/tributIssLista':
			  return MaterialPageRoute(builder: (_) => TributIssListaPage());
			case '/tributIssPersiste':
			  return MaterialPageRoute(builder: (_) => TributIssPersistePage());
			
			// TributOperacaoFiscal
			case '/tributOperacaoFiscalLista':
			  return MaterialPageRoute(builder: (_) => TributOperacaoFiscalListaPage());
			case '/tributOperacaoFiscalPersiste':
			  return MaterialPageRoute(builder: (_) => TributOperacaoFiscalPersistePage());
			

      ////////////////////////////////////////////////////////// 
      /// CONTROLE DE ESTOQUE
      //////////////////////////////////////////////////////////

			// EstoqueReajusteCabecalho
			case '/estoqueReajusteCabecalhoLista':
			  return MaterialPageRoute(builder: (_) => EstoqueReajusteCabecalhoListaPage());
			case '/estoqueReajusteCabecalhoPersiste':
			  return MaterialPageRoute(builder: (_) => EstoqueReajusteCabecalhoPersistePage());

			// RequisicaoInternaCabecalho
			case '/requisicaoInternaCabecalhoLista':
			  return MaterialPageRoute(builder: (_) => RequisicaoInternaCabecalhoListaPage());
			case '/requisicaoInternaCabecalhoPersiste':
			  return MaterialPageRoute(builder: (_) => RequisicaoInternaCabecalhoPersistePage());


      ////////////////////////////////////////////////////////// 
      /// VENDAS
      //////////////////////////////////////////////////////////

			// NotaFiscalModelo
			case '/notaFiscalModeloLista':
			  return MaterialPageRoute(builder: (_) => NotaFiscalModeloListaPage());
			case '/notaFiscalModeloPersiste':
			  return MaterialPageRoute(builder: (_) => NotaFiscalModeloPersistePage());
			
			// NotaFiscalTipo
			case '/notaFiscalTipoLista':
			  return MaterialPageRoute(builder: (_) => NotaFiscalTipoListaPage());
			case '/notaFiscalTipoPersiste':
			  return MaterialPageRoute(builder: (_) => NotaFiscalTipoPersistePage());

			// VendaCabecalho
			case '/vendaCabecalhoLista':
			  return MaterialPageRoute(builder: (_) => VendaCabecalhoListaPage());
			case '/vendaCabecalhoPersiste':
			  return MaterialPageRoute(builder: (_) => VendaCabecalhoPersistePage());
			
			// VendaCondicoesPagamento
			case '/vendaCondicoesPagamentoLista':
			  return MaterialPageRoute(builder: (_) => VendaCondicoesPagamentoListaPage());
			case '/vendaCondicoesPagamentoPersiste':
			  return MaterialPageRoute(builder: (_) => VendaCondicoesPagamentoPersistePage());
									
			// VendaFrete
			case '/vendaFreteLista':
			  return MaterialPageRoute(builder: (_) => VendaFreteListaPage());
			case '/vendaFretePersiste':
			  return MaterialPageRoute(builder: (_) => VendaFretePersistePage());
			
			// VendaOrcamentoCabecalho
			case '/vendaOrcamentoCabecalhoLista':
			  return MaterialPageRoute(builder: (_) => VendaOrcamentoCabecalhoListaPage());
			case '/vendaOrcamentoCabecalhoPersiste':
			  return MaterialPageRoute(builder: (_) => VendaOrcamentoCabecalhoPersistePage());

			  
      ////////////////////////////////////////////////////////// 
      /// COMPRAS
      //////////////////////////////////////////////////////////

			// CompraCotacao
			case '/compraCotacaoLista':
			  return MaterialPageRoute(builder: (_) => CompraCotacaoListaPage());
			case '/compraCotacaoPersiste':
			  return MaterialPageRoute(builder: (_) => CompraCotacaoPersistePage());
			
			// CompraPedido
			case '/compraPedidoLista':
			  return MaterialPageRoute(builder: (_) => CompraPedidoListaPage());
			case '/compraPedidoPersiste':
			  return MaterialPageRoute(builder: (_) => CompraPedidoPersistePage());
			
			// CompraRequisicao
			case '/compraRequisicaoLista':
			  return MaterialPageRoute(builder: (_) => CompraRequisicaoListaPage());
			case '/compraRequisicaoPersiste':
			  return MaterialPageRoute(builder: (_) => CompraRequisicaoPersistePage());
			
			// CompraTipoPedido
			case '/compraTipoPedidoLista':
			  return MaterialPageRoute(builder: (_) => CompraTipoPedidoListaPage());
			case '/compraTipoPedidoPersiste':
			  return MaterialPageRoute(builder: (_) => CompraTipoPedidoPersistePage());
			
			// CompraTipoRequisicao
			case '/compraTipoRequisicaoLista':
			  return MaterialPageRoute(builder: (_) => CompraTipoRequisicaoListaPage());
			case '/compraTipoRequisicaoPersiste':
			  return MaterialPageRoute(builder: (_) => CompraTipoRequisicaoPersistePage());


      ////////////////////////////////////////////////////////// 
      /// COMISSÕES
      //////////////////////////////////////////////////////////

			// ComissaoObjetivo
			case '/comissaoObjetivoLista':
			  return MaterialPageRoute(builder: (_) => ComissaoObjetivoListaPage());
			case '/comissaoObjetivoPersiste':
			  return MaterialPageRoute(builder: (_) => ComissaoObjetivoPersistePage());
			
			// ComissaoPerfil
			case '/comissaoPerfilLista':
			  return MaterialPageRoute(builder: (_) => ComissaoPerfilListaPage());
			case '/comissaoPerfilPersiste':
			  return MaterialPageRoute(builder: (_) => ComissaoPerfilPersistePage());


      ////////////////////////////////////////////////////////// 
      /// ORDEM DE SERVIÇO
      //////////////////////////////////////////////////////////

			// OsAbertura
			case '/osAberturaLista':
			  return MaterialPageRoute(builder: (_) => OsAberturaListaPage());
			case '/osAberturaPersiste':
			  return MaterialPageRoute(builder: (_) => OsAberturaPersistePage());
						
			// OsEquipamento
			case '/osEquipamentoLista':
			  return MaterialPageRoute(builder: (_) => OsEquipamentoListaPage());
			case '/osEquipamentoPersiste':
			  return MaterialPageRoute(builder: (_) => OsEquipamentoPersistePage());
						
			// OsStatus
			case '/osStatusLista':
			  return MaterialPageRoute(builder: (_) => OsStatusListaPage());
			case '/osStatusPersiste':
			  return MaterialPageRoute(builder: (_) => OsStatusPersistePage());

      ////////////////////////////////////////////////////////// 
      /// AFV
      //////////////////////////////////////////////////////////

      //Visão Vendedor
			case '/visaoVendedor':
			  return MaterialPageRoute(builder: (_) => ViewPessoaClientePage());

			// TabelaPreco
			case '/tabelaPrecoLista':
			  return MaterialPageRoute(builder: (_) => TabelaPrecoListaPage());
			case '/tabelaPrecoPersiste':
			  return MaterialPageRoute(builder: (_) => TabelaPrecoPersistePage());

			// VendedorMeta
			case '/vendedorMetaLista':
			  return MaterialPageRoute(builder: (_) => VendedorMetaListaPage());
			case '/vendedorMetaPersiste':
			  return MaterialPageRoute(builder: (_) => VendedorMetaPersistePage());
			
			// VendedorRota
			case '/vendedorRotaLista':
			  return MaterialPageRoute(builder: (_) => VendedorRotaListaPage());
			case '/vendedorRotaPersiste':
			  return MaterialPageRoute(builder: (_) => VendedorRotaPersistePage());

      ////////////////////////////////////////////////////////// 
      /// NFS-e
      //////////////////////////////////////////////////////////
			// NfseCabecalho
			case '/nfseCabecalhoLista':
			  return MaterialPageRoute(builder: (_) => NfseCabecalhoListaPage());
			case '/nfseCabecalhoPersiste':
			  return MaterialPageRoute(builder: (_) => NfseCabecalhoPersistePage());
						
			// NfseListaServico
			case '/nfseListaServicoLista':
			  return MaterialPageRoute(builder: (_) => NfseListaServicoListaPage());
			case '/nfseListaServicoPersiste':
			  return MaterialPageRoute(builder: (_) => NfseListaServicoPersistePage());


      ////////////////////////////////////////////////////////// 
      /// NF-e
      //////////////////////////////////////////////////////////
			// NfeCabecalho
			case '/nfeCabecalhoLista':
			  return MaterialPageRoute(builder: (_) => NfeCabecalhoListaPage());
			case '/nfeCabecalhoPersiste':
			  return MaterialPageRoute(builder: (_) => NfeCabecalhoPersistePage());



			  
      // Será Implementado
      case '/seraImplementado':
        return MaterialPageRoute(builder: (_) => SeraImplementadoPage());


		
      // default
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
              body: Center(
            child: Text('Nenhuma rota definida para {$settings.name}'),
          )),
        );
    }
  }
}
